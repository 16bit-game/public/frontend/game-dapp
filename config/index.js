export default {
  API: (network = 'testnet') => {
    return `https://api.16bit.game/${network}/v1`;
  },
  WS: 'ws.16bit.game',
  EOS: {
    NODE: {
      testnet: 'testnet.16bit.game',
      mainnet: 'api.main.alohaeos.com'
    },
    NODE_UTILITY: 'https://api.main.alohaeos.com',
    CONTRACT: 'fairbet.game',
    REG_CONTRACT: 'popcornmaker'
  },
  rpcNodes: [
    {
      name: 'Aloha EOS',
      protocol: 'https',
      host: 'api.main.alohaeos.com',
      port: 443,
      weight: 1,
      backup: false
    }, {
      name: 'EOS Nation',
      protocol: 'https',
      host: 'api.eosn.io',
      port: 443,
      weight: 1,
      backup: false
    }
  ]
}
