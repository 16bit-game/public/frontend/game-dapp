import Vue from 'vue';
import VueAnalytics from 'vue-analytics';


Vue.use(VueAnalytics, {
  id: ['UA-173624572-3']
});

const addPostfix = (label) => {
  const lang = localStorage.getItem('lang');
  switch (lang) {
    case 'en': {
      return label;
    }
    case 'ch': {
      return label + 'ch';
    }
    case 'esp': {
      return label + 'esp';
    }
    case 'ru': {
      return label + 'ru';
    }
    default: {
      return label;
    }
  }
};

const analyticPlugin = {
  install(app) {
    Vue.prototype.ga = (action, formatAction = false) => {
      return app.$ga.event(addPostfix('press'), formatAction ? addPostfix(action) : action);
    }
  }
};

Vue.use(analyticPlugin);
