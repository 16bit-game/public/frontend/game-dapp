import Vue from 'vue'
import VTooltip from 'v-tooltip'


const wrapperScroll = {
  container: null,
  element: null,
  setContainer(el) {
    this.container = el;
  },
  flush() {
    this.element = null;
  },
  scroll(e) {
    if (this.container && this.element) {
      this.element(e);
    }
  },
  onScroll(cb) {
    this.element = cb;
  }
};

const scrollPlugin = {
  install() {
    Vue.prototype.$onWrapperScroll = wrapperScroll;
  }
};
Vue.use(scrollPlugin);
Vue.use(VTooltip);

