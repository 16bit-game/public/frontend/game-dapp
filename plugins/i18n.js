import Vue from 'vue';
import VueI18n from 'vue-i18n';
import modal from 'vue-js-modal'
import getUserLocale from 'get-user-locale';
import store from './../store/common';

Vue.use(VueI18n);
Vue.use(modal);


export default async ({app, store}) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  let locale = localStorage.getItem("lang") || getUserLocale();
  let first = locale.split('-')[0];
  if (!localStorage.getItem("lang")) {
    let [err, res] = await store.dispatch('common/getIpInfo');
    if (!err && res) {
      first = res.country;
    }
  }
  locale = `${first}-${first.toUpperCase()}`;

  store.commit('common/SET_LANGUAGE', locale);
  app.i18n = new VueI18n({
    locale: store.state.common.locale,
    fallbackLocale: 'ru-RU',
    messages: {
      'en-EN': require('~/locales/en.json'),
      'ru-RU': require('~/locales/ru.json'),
      'ch-CH': require('~/locales/ch.json'),
      'es-ES': require('~/locales/esp.json')
    }
  });

  app.i18n.path = (link) => {
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`
    }

    return `/${app.i18n.locale}/${link}`
  }
}
