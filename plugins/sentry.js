import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';
import { Integrations } from "@sentry/tracing";

Sentry.init({
  dsn: "https://d466828459054a77af234f43fadea3dd@sentry.16bit.game/2",
  integrations: [
    new VueIntegration({
      Vue,
      tracing: true,
      tracingOptions: {
        trackComponents: true,
      },
    }),
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

Sentry.configureScope(function (scope) {
  scope.setUser({"username": localStorage.getItem('accountname') || 'No account'});
});


const sentryClient = {
  install() {
    Vue.prototype.$sentry = Sentry;
  }
};
Vue.use(sentryClient);
