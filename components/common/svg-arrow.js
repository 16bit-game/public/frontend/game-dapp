import {SVG} from '@svgdotjs/svg.js'

class SvgArrow {
  constructor(element) {
    this._draw = SVG().addTo(element).size('100%', '100%');
    this._settings = {
      dasharray: '4,3',
      color: 'rgb(0, 189, 197)',
      arrowSize: 10
    }
  }

  _drawSteps() {
    const steps = document.querySelectorAll('.instruction .instruction-step');
    const startCoords = steps[0].getClientRects()[0];
    const verticalDiff = steps[1].getClientRects()[0].top - startCoords.top;
    steps.forEach((item, index) => {
      const left = item.offsetLeft + item.offsetWidth / 2;
      const top = item.offsetTop + item.offsetHeight;
      this._draw.line(left, top, left, top + verticalDiff).stroke({
        width: 1,
        color: this._settings.color,
        dasharray: this._settings.dasharray
      });
      if (index === steps.length - 1) {
        this._drawFinishStep(left);
      }
    });
  }

  _drawFinishStep(left) {
    const el = document.querySelector('.checkResult_wolfram .info-mockup');
    if (!el) return;
    const top = el.offsetTop + el.offsetWidth / 2;
    const finishLeft = el.offsetLeft - this._settings.arrowSize;
    this._draw.line(left, top, finishLeft, top).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.polygon(`${finishLeft}, ${top - this._settings.arrowSize / 2}, ${finishLeft}, ${top + this._settings.arrowSize / 2}, ${finishLeft + this._settings.arrowSize / 2}, ${top}`).fill('rgb(0, 189, 197)').stroke({width: 1})
  }

  _drawBetweenElements() {
    const els = document.querySelectorAll('.checkResult_wolfram .info-mockup');
    const gear = document.querySelector('.checkResult_wolfram .gear');
    if (!els[1] || !els[2] || !gear) return;
    const top = els[1].offsetTop + els[1].offsetHeight / 2;
    const e1Right = els[1].offsetLeft + els[1].offsetWidth;
    const e2Left = els[2].offsetLeft - this._settings.arrowSize;
    const gLeft = gear.offsetLeft - 3;
    const gRight = gear.offsetLeft + gear.offsetWidth;
    this._draw.line(e1Right, top, gLeft, top).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.line(gRight, top, e2Left, top).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.polygon(`${e2Left}, ${top - this._settings.arrowSize / 2}, ${e2Left}, ${top + this._settings.arrowSize / 2}, ${e2Left + this._settings.arrowSize / 2}, ${top}`).fill(this._settings.color).stroke({width: 1})
  }


  init() {
    this._drawSteps();
    this._drawBetweenElements();
  }
}

export default SvgArrow;
