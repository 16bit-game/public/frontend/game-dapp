import {SVG} from '@svgdotjs/svg.js'

class SvgArrow {
  constructor(element) {
    this._el = element;
    this._draw = null;
    this._settings = {
      dasharray: '4,3',
      color: 'rgb(0, 189, 197)',
      arrowSize: 10,
      offset: 5
      //color: 'red'
    }
  }

  _drawSteps() {
    const steps = document.querySelectorAll('.history_details-info .info-mockup');
    const startCoordsLeft = steps[1].offsetLeft + steps[1].offsetWidth / 2;
    const startCoordsTop = steps[1].offsetTop + steps[1].offsetHeight + this._settings.offset;
    const endCoordsLeft = steps[2].offsetLeft - this._settings.arrowSize * 2 - this._settings.offset;
    const endCoordsTop = steps[2].offsetTop + steps[2].offsetHeight / 2;
    const l1 = this._draw.line(startCoordsLeft, startCoordsTop, startCoordsLeft, endCoordsTop).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.line(startCoordsLeft, endCoordsTop, endCoordsLeft, endCoordsTop).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.polygon(`${endCoordsLeft}, ${endCoordsTop - this._settings.arrowSize / 2}, ${endCoordsLeft}, ${endCoordsTop + this._settings.arrowSize / 2}, ${endCoordsLeft + this._settings.arrowSize / 2}, ${endCoordsTop}`).fill(this._settings.color).stroke({width: 1})
  }

  _drawBetweenElements() {
    const els = document.querySelectorAll('.history_details-info')[2].querySelectorAll('.info-mockup');
    const gear = document.querySelector('.history_details-info .gear');
    if (!els[1] || !els[2] || !gear) return;
    const top = els[1].offsetTop + els[1].offsetHeight / 2;
    const e1Right = els[1].offsetLeft + this._settings.offset * 2;
    const e2Left = els[2].offsetLeft - this._settings.arrowSize;
    const gLeft = gear.offsetLeft - this._settings.offset * 3;
    const gRight = gear.offsetLeft + gear.offsetWidth;
    this._draw.line(e1Right, top, gLeft, top).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.line(gRight, top, e2Left, top).stroke({
      width: 1,
      color: this._settings.color,
      dasharray: this._settings.dasharray
    });
    this._draw.polygon(`${e2Left}, ${top - this._settings.arrowSize / 2}, ${e2Left}, ${top + this._settings.arrowSize / 2}, ${e2Left + this._settings.arrowSize / 2}, ${top}`).fill(this._settings.color).stroke({width: 1})
  }


  init() {
    this._draw = SVG().addTo(this._el).size('100%', '100%');
    this._drawSteps();
    this._drawBetweenElements();
  }
  clear() {
    this._draw && this._draw.clear();
  }
}

export default SvgArrow;
