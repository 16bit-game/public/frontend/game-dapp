import axios from 'axios';
import {errorHandler} from './utility/';

import config from '~/config/';

const service = axios.create({
  baseURL: config.API(),
  timeout: 5000 * 6
});
service.interceptors.request.use(
  config => {
    //console.log('REQUEST - ', config);
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.code > 399) {
      const er = res.error || res.errors;
      if (!er) {
        errorHandler(res.code);
      }
      return er ? Promise.reject(er) : Promise.reject(`Something went wrong ${res.code}`);
    } else {
      return res.payload;
    }
  },
  error => {
    /*notification.error({
      message: res.error || 'Something went wrong',
      position: 'bottom-left',
    });*/
    console.log('err' + error);
    return Promise.reject(error);
  }
);

export default service;
