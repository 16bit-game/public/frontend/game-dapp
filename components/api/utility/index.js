import notification
  from '~/components/UI/Notification/main.js'

export const errorHandler = (code) => {
  switch (code) {
    case 500: {
      notification.error({
        message: `Something went wrong`,
      });
      break;
    }
    case 10001: {
      notification.info({
        message: `Game has been ended`,
      });
      break;
    }
    case 10002: {
      notification.error({
        message: `Amount too low`,
      });
      break;
    }
    case 10003: {
      notification.error({
        message: `Game's max cap has been achieved`,
      });
      break;
    }
  }
};
