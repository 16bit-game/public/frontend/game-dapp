import req from './req';
import config from '~/config';

export const createUser = ({net, ...data} = {}) => req({
  url: `${config.API(net)}/account/registration`,
  method: 'post',
  data
});

export const createUser_new = ({net, ...data} = {}) => {
  return req({
    url: `/testnet/v1/payment`,
    method: 'post',
    data
  });
};

export const fauset = (username) => req({
  url: `/account/${username}/fauset`,
  method: 'get'
});

export const fausetBonus = (username) => req({
  url: `/account/${username}/fauset/bonus`,
  method: 'get'
});

export const sign = (data) => req({
  url: `/account/${data.username}/bonus`,
  method: 'POST',
  data
});

export const fetchReferralLink = (data) =>req({
  url: `https://16bit.promo/shorturl`,
  method: 'get',
  params: data
});

export const fetchReferralInfo = () => req({
  url: `https://16bit.promo/refs`,
  withCredentials: true,
  method: 'GET'
});

export const addUserInfo = (data) => req({
  url: `/referral`,
  method: 'post',
  withCredentials: true,
  data
});
