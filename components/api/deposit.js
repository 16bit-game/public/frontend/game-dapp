import req from './req';
import axios from 'axios';

export const deposit = ({username, ...data}) => req({
  url: `https://api.16bit.game/mainnet/v1/account/${username}/deposit`,
  method: 'POST',
  data
});

export const estimated = ({amount, from}) => req({
  url: `https://api.16bit.game/mainnet/v1/account/deposit/${amount}/${from}/estimation`,
  method: 'GET'
});

export const minRate = (from) => axios.get(`https://changenow.io/api/v1/min-amount/${from}_eos`);

export const rates = () => axios.get('https://changenow.io/api/v1/currencies-to/eos');

export const processing = (data) => req({
  url: `https://api.16bit.game/mainnet/v1/processing`,
  method: 'POST',
  data
});

export const processingSwitchere = (data) => req({
  url: `https://api.16bit.game/mainnet/v1/switchere`,
  method: 'POST',
  data
});


