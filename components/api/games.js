import req from './req';

export const fetchAll = ({username, byUser, ...data}) => req({
  url: (byUser ? '/player/' + username : '') + '/games' + (username ? '?username=' + username : ''),
  method: 'get',
  params: data
});

export const fetchOne = ({hash, username, other = ''} = {}) => req({
  url: `/game/${hash}` + (username ? '/' + username : '') + '/bets' + other,
  method: 'get'
});

export const fetchOneGame = (hash) => req({
  url: `/game/${hash}`,
  method: 'get'
});

export const fetchOneByAll = ({hash} = {}) => req({
  url: `/game/${hash}/bets`,
  method: 'get'
});

export const fetchLast = (data) => {
  return req({
    url: '/games/last',
    method: 'get',
    params: data
  });
};

export const fetchSettings = (network) => req({
  url: `https://api.16bit.partners/${network}/v1/settings`,
  method: 'get'
});
