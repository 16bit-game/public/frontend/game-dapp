import req from "./req";

export const fetch = (data) => req({
  url: `/players`,
  method: 'get',
  params: data
});

export const fetchCount = (username) => req({
  url: `/player/${username}/referrals/count`,
  method: 'get',
});

export const fetchReferrals = ({username, ...data}) => req({
  url: `/player/${username}/referrals`,
  method: 'get',
  params: data
});

export const fetchAllReferrals = (data) => req({
  url: `/players/referrals`,
  method: 'get',
  params: data
});

export const fetchPlayerRating = ({username, today = ''} = {}) => req({
  url: `/player/${username}/rating/${today}`,
  method: 'get'
});

export const fetchPlayerGlobalReferral = (username) => req({
  url: `/referrals/${username}`,
  method: 'get'
});
