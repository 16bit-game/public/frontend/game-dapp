import req from './req';

export const fetch = () => req({
  url: '/bet',
  method: 'get'
});

export const create = ({house_seed_hash, username, ...data}) => {
  return req({
    url: `/game/${house_seed_hash}/${username}/bet`,
    method: 'post',
    data
  });
};

export const createSignedBet = (data) => {
  return req({
    url: `/sign/bet`,
    method: 'post',
    data
  });
};
