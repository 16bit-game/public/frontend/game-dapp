import axios from 'axios';

const checker = {
  url: '',
  accountName: '',
  _timer: 20000,
  _interval: null,
  enable() {
    this._interval = setInterval(_ => {
      this.url && this.post();
    }, this._timer);
  },
  async post() {
    if (!this.accountName) return;
    try {
      const res = await axios.post(this.url, {
        account_name: this.accountName
      });
      postMessage({
        success: true,
        data: res.data
      });
    } catch (e) {
      console.log('error - ', e);
      postMessage({
        success: false,
        data: e
      });
    }
  },
  setOpitons({url, accountName}) {
    this.url = url;
    this.accountName = accountName;
    if (!this._interval) {
      this.post();
      this.enable();
    }
  },
  clear() {
    this.url = '';
    this.accountName = '';
    clearInterval(this._interval);
    this._interval = null;
  }
};


onmessage = (msg) => {
  switch (msg.data.msg) {
    case 'check': {
      const {url, accountName} = msg.data;
      checker.setOpitons({url, accountName});
      break;
    }
    case 'clear': {
      checker.clear();
      break;
    }
  }
};
