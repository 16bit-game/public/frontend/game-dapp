import {create} from "~/components/api/bet";


const bet = ({house_seed_hash, username, number, amount, timestamp, language, signature, referral} = {}) => new Promise((resolve, reject) => {
  return new Promise((resolve, reject) => {
    create({
      house_seed_hash,
      username,
      number,
      amount,
      timestamp,
      language,
      signature: '',
      referral: ''
    }).then(_ => {
      postMessage({
        type: 'success'
      });
      return resolve();
    }).catch(e => {
      postMessage({
        type: 'createBetError',
        payload: e
      });
      return reject(e);
    });
  })
});


onmessage = (msg) => {
  bet(msg.data);
};
