import {flooring} from "./index";

export const waitingCellStorage = {
  settings: {
    ttl: 0,
    cap: 240, //Dont forget color mixer
    step: 1,
    cell_limit: 5
  },
  storage: {},
  add(bet) {
    if (!this.storage[bet.username]) {
      this.storage[bet.username] = {
        totalAmount: 0
      }
    }
    if (!this.storage[bet.number[0]]) {
      this.storage[bet.number[0]] = {};
    }
    if (!this.storage[bet.username][bet.number[0]]) {
      this.storage[bet.username][bet.number[0]] = {
        waitingAmount: 0
      }
    }
    if (!this.storage[bet.number[0]][bet.username]) {
      this.storage[bet.number[0]][bet.username] = {
        waitingAmount: 0
      }
    }
    const isValid = this._checkRules(bet);
    if (!isValid) return false;
    this.storage[bet.username][bet.number[0]].waitingAmount = flooring(this.storage[bet.username][bet.number[0]].waitingAmount + bet.amount);
    this.storage[bet.username].totalAmount = flooring(this.storage[bet.username].totalAmount + bet.amount);
    return this._maxBet(bet);
  },
  remove(bet) {
    if (this.storage[bet.username] && this.storage[bet.username][bet.number[0]]) {
      this.storage[bet.username][bet.number[0]].waitingAmount = flooring(this.storage[bet.username][bet.number[0]].waitingAmount - bet.amount);
      this.storage[bet.username].totalAmount = flooring(this.storage[bet.username].totalAmount + bet.amount);
    }
  },
  _maxBet(bet) {
    if (this.storage[bet.number[0]]) {
      return Object.keys(this.storage[bet.number[0]]).length + this.settings.cell_limit - 1;
    } else {
      return this.settings.cell_limit;
    }
  },
  _checkRules(bet) {
    let totalAmount = 0;
    if (bet.amount < this.settings.step) return false;
    if (this.storage[bet.username][bet.number[0]].waitingAmount + bet.amount > this.settings.cell_limit) return false;
    const storage = Object.keys(this.storage);
    storage.forEach(key => {
      totalAmount += this.storage[key].totalAmount;
    });
    if (totalAmount >= this.settings.cap) return false;
    return true;
  },
  availableForBet({number, amount, username} = {}) {
    if (this.storage[username] && this.storage[username][number]) {
      return this.storage[username][number].waitingAmount < this.settings.cell_limit;
    } else {
      return true;
    }
  },
  getDiffForBet({number, username} = {}) {
    if (this.storage[username] && this.storage[username][number]) {
      return this.storage[username][number].waitingAmount;
    } else {
      return 0;
    }
  },
  clear() {
    const storage = Object.keys(this.storage);
    storage.forEach(key => {
      Object.values(this.storage[key]).forEach(item => item = null);
    });
    this.storage = {};
  }
};
