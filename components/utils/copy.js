import notification
  from '~/components/UI/Notification/main.js'

let notif = null;

export default (text = '', message = 'Copied!') => {
  let inp = document.createElement('textarea');
  inp.value = text;
  inp.classList.add('forselect');
  inp.style.position = 'fixed';
  inp.style.top = '-100%';
  inp.style.left = '-100%';
  document.body.appendChild(inp);
  let data = document.querySelector('textarea.forselect');
  if (document.createRange) {
    data.select();
    document.execCommand('copy');
    notif && notif.close();
    notif = notification.info({
      message: message,
    });
  }
  data.remove();
}
