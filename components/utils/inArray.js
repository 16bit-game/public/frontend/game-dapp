export default (array, val) => {
  let exist = false;
  array.forEach(item => {
    if (item.number[0] === val) exist = true;
  });
  return exist;
};
