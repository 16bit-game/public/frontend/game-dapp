import FingerPrint from 'fingerprintjs2';


export const mergeByDays = ({data, year, month}) => {
  let d = [];
  const days = new Date(year, month, 0).getDate();
  if (typeof days !== 'number') return data;
  for (let i = 1; i <= data[data.length - 1].day; i++) {
    const el = data.filter(item => item.day === i);
    if (el && el.length) {
      d.push(el[0]);
    } else {
      d.push({
        day: i,
        stats: {
          player: {
            bets_amount: 0,
            losses: {
              amount: 0,
              count: 0
            },
            wins: {
              amount: 0,
              count: 0
            }
          },
          total: {
            bets_amount: 0,
            losses: {
              amount: 0,
              count: 0
            },
            wins: {
              amount: 0,
              count: 0
            }
          }
        }
      });
    }
  }
  return d;
};

export const getScrollPercent = (element = document.documentElement) => {
  let h = element,
    st = 'scrollTop',
    sh = 'scrollHeight';
  return (h[st]) / ((h[sh]) - h.clientHeight) * 100;
};

export const download = (filename, text) => {
  const element = document.createElement('a');
  element.setAttribute('target', '_blank');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  //element.setAttribute('target', '_blank');

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
};

export const toHex = (string) => {
  let hex, i;

  let result = "";
  for (i = 0; i < string.length; i++) {
    hex = string.charCodeAt(i).toString(16);
    result += ("000" + hex).slice(-4);
  }
  return result
};

export const getFingerPrint = () => new Promise((resolve) => {
  FingerPrint.get((components) => {
    components.push({
      key: "timestamp",
      value: new Date().getTime()
    });
    components.push({
      key: "math_seed",
      value: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    });
    resolve(components.map(item => item.value).join(''));
  });
});

export const scrollTo = (element, to, duration) => {
  if (duration <= 0) return;
  const difference = to - element.scrollTop;
  let perTick = difference / duration * 10;

  setTimeout(function () {
    element.scrollTop = element.scrollTop + perTick;
    if (element.scrollTop === to) return;
    scrollTo(element, to, duration - 10);
  }, 10);
};

export const flooring = (amount) => Math.round(amount * 10000) / 10000;
