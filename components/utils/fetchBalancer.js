const request  = require('superagent');
const Throttle = require('superagent-throttle')
const Balancer = require('superagent-load-balancer');

class FetchBalancer {
  constructor({
                endpoints = [],
                type = 'round-robin',
                healthCheckOptions = {
                  interval: 60000,
                  window: 5,
                  threshold: 3,
                  timeout: {
                    response: 2000,
                    deadline: 5000
                  },
                  throttle: {
                    active: true,
                    rate: 20,
                    ratePer: 10000,
                    concurrent: 2
                  }
                },
                upstreamOptions = {
                  retry: 2,
                  timeout: {
                    response: 2000,
                    deadline: 5000
                  }
                },
                logger = _ => {}
              } = {}) {
    this.log = (...rest) => logger('EOSJS Fetch Balancer', ' – ', ...rest);
    this.log('initialization');

    const throttle = new Throttle(healthCheckOptions.throttle);
    const balancer = new Balancer(endpoints, type);
    balancer.on('healthy', endpoint => this.log('endpoint status: HEALTHY', endpoint));
    balancer.on('sick', endpoint => this.log('endpoint status: OUTAGE', endpoint));
    balancer.on('hit', endpoint => this.log('endpoint HIT', endpoint));

    this.isRpcError = json => {
      return !json || json.error || (json.processed && json.processed.except);
    };

    this.healthCheck = balancer.startHealthCheck(Object.assign({
      ping: endpoint => {
        this.log('endpoint HEALTHCHECK', endpoint);
        const url = `${endpoint.protocol || 'http'}://${endpoint.host || endpoint.ip}:${endpoint.port}/v1/chain/get_abi`;
        return new Promise((resolve, reject) => {
          request
            .post(url)
            .type('json').accept('json')
            .send({account_name: 'eosio'})
            .timeout(healthCheckOptions.timeout)
            .use(throttle.plugin())
            .then(res => !this.isRpcError(res.body) ? resolve() : reject())
            .catch(reject);
        });
      }
    }, healthCheckOptions));

    this.request = request.agent()
      .use(balancer.plugin())
      .type('json').accept('json')
      .retry(upstreamOptions.retry, (err, res) => !!err || this.isRpcError(res.body))
      .timeout(upstreamOptions.timeout);
    this.log('ready', balancer.getAvailableServers());

    return Object.assign(
      (path, {method = 'POST', body = {}} = {}) => {
        return new Promise((resolve, reject) => {
          this.request[method.toLowerCase()](path)
            .send(body)
            .then(res => {
              resolve({
                ...res,
                json: _ => Promise.resolve(res.body)
              });
            }).catch(reject);
        });
      }, {
        destroy: _ => {
          return new Promise(resolve => {
            clearInterval(this.healthCheck);
            setTimeout(_ => {
              this.healthCheck = null;
              this.isRpcError = null;
              this.request = null;
              this.log = null;
              resolve();
            }, healthCheckOptions.interval);
          });
        }
      }
    );
  }
}

module.exports = FetchBalancer;
