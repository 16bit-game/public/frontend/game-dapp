import RandExp from 'randexp';

const mixin_signIn = {
  methods: {
    mixin_checkOrGenerateName(accountName = '', maxLength = 12, network = 'testnet', needToCommit = true) {
      return new Promise((resolve, reject) => {
        const regName = new RandExp(/[a-z1-5]{12}/);
        const name = accountName || regName.gen();
        const gen = () => {
          this.$store.dispatch('signIn/checkUserName', {
            name,
            network,
            needToCommit
          }).then(res => {
            return gen();
          }).catch(e => {
            return resolve(name);
          });
        };
        gen();
      });
    }
  }
};

export default mixin_signIn;
