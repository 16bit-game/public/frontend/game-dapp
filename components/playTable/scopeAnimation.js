import {SVG} from '@svgdotjs/svg.js'

const roundPathCorners = (pathString, radius, useFractionalRadius = false) => {
  const moveTowardsLength = (movingPoint, targetPoint, amount) => {
    const width = (targetPoint.x - movingPoint.x);
    const height = (targetPoint.y - movingPoint.y);

    const distance = Math.sqrt(width * width + height * height);

    return moveTowardsFractional(movingPoint, targetPoint, Math.min(1, amount / distance));
  };

  const moveTowardsFractional = (movingPoint, targetPoint, fraction) => {
    return {
      x: movingPoint.x + (targetPoint.x - movingPoint.x) * fraction,
      y: movingPoint.y + (targetPoint.y - movingPoint.y) * fraction
    };
  }

  const adjustCommand = (cmd, newPoint) => {
    if (cmd.length > 2) {
      cmd[cmd.length - 2] = newPoint.x;
      cmd[cmd.length - 1] = newPoint.y;
    }
  }

  const pointForCommand = (cmd) => {
    return {
      x: parseFloat(cmd[cmd.length - 2]),
      y: parseFloat(cmd[cmd.length - 1]),
    };
  }

  const pathParts = pathString.split(/[,\s]/).reduce((parts, part) => {
    const match = part.match('([a-zA-Z])(.+)');
    if (match) {
      parts.push(match[1]);
      parts.push(match[2]);
    } else {
      parts.push(part);
    }

    return parts;
  }, []);

  const commands = pathParts.reduce(function (commands, part) {
    if (parseFloat(part) == part && commands.length) {
      commands[commands.length - 1].push(part);
    } else {
      commands.push([part]);
    }

    return commands;
  }, []);

  let resultCommands = [];

  if (commands.length > 1) {
    const startPoint = pointForCommand(commands[0]);

    let virtualCloseLine = null;
    if (commands[commands.length - 1][0] == 'Z' && commands[0].length > 2) {
      virtualCloseLine = ['L', startPoint.x, startPoint.y];
      commands[commands.length - 1] = virtualCloseLine;
    }

    resultCommands.push(commands[0]);

    for (let cmdIndex = 1; cmdIndex < commands.length; cmdIndex++) {
      const prevCmd = resultCommands[resultCommands.length - 1];

      const curCmd = commands[cmdIndex];

      // Handle closing case
      const nextCmd = (curCmd == virtualCloseLine)
        ? commands[1]
        : commands[cmdIndex + 1];

      // Nasty logic to decide if this path is a candidite.
      if (nextCmd && prevCmd && (prevCmd.length > 2) && curCmd[0] == 'L' && nextCmd.length > 2 && nextCmd[0] == 'L') {
        const prevPoint = pointForCommand(prevCmd);
        const curPoint = pointForCommand(curCmd);
        const nextPoint = pointForCommand(nextCmd);

        let curveStart;
        let curveEnd;

        if (useFractionalRadius) {
          curveStart = moveTowardsFractional(curPoint, prevCmd.origPoint || prevPoint, radius);
          curveEnd = moveTowardsFractional(curPoint, nextCmd.origPoint || nextPoint, radius);
        } else {
          curveStart = moveTowardsLength(curPoint, prevPoint, radius);
          curveEnd = moveTowardsLength(curPoint, nextPoint, radius);
        }

        adjustCommand(curCmd, curveStart);
        curCmd.origPoint = curPoint;
        resultCommands.push(curCmd);

        const startControl = moveTowardsFractional(curveStart, curPoint, .5);
        const endControl = moveTowardsFractional(curPoint, curveEnd, .5);

        const curveCmd = ['C', startControl.x, startControl.y, endControl.x, endControl.y, curveEnd.x, curveEnd.y];
        curveCmd.origPoint = curPoint;
        resultCommands.push(curveCmd);
      } else {
        resultCommands.push(curCmd);
      }
    }

    if (virtualCloseLine) {
      const newStartPoint = pointForCommand(resultCommands[resultCommands.length - 1]);
      resultCommands.push(['Z']);
      adjustCommand(resultCommands[0], newStartPoint);
    }
  } else {
    resultCommands = commands;
  }

  return resultCommands.reduce(function (str, c) {
    return str + c.join(' ') + ' ';
  }, '');
};

const canMoveRight = (cell) => {
  return cell.links.right && fieldAnimations.field[cell.links.right];
};
const canMoveLeft = (cell) => {
  return cell.links.left && fieldAnimations.field[cell.links.left];
};
const canMoveTop = (cell) => {
  return cell.links.top && fieldAnimations.field[cell.links.top];
};
const canMoveBottom = (cell) => {
  return cell.links.bottom && fieldAnimations.field[cell.links.bottom];
};

const getTopLeftPoint = (cell) => {
  return {
    x: cell.points.left,
    y: cell.points.top
  }
};
const getTopRightPoint = (cell) => {
  return {
    x: cell.points.right,
    y: cell.points.top
  }
};
const getBottomRightPoint = (cell) => {
  return {
    x: cell.points.right,
    y: cell.points.bottom
  }
};
const getBottomLeftPoint = (cell) => {
  return {
    x: cell.points.left,
    y: cell.points.bottom
  }
};
const getLastPoint = (cell, last) => {
  if (!last.cellKey) return false;
  const lastKey = last.cellKey;
  if (cell.links.top === lastKey) return 'top';
  if (cell.links.right === lastKey) return 'right';
  if (cell.links.bottom === lastKey) return 'bottom';
  if (cell.links.left === lastKey) return 'left';
};

const fieldAnimations = {
  field: {},
  _settings: {
    color: '#0001f6',
    //color: 'rgb(255, 0, 0)',
    padding: 8,
    width: 1
  },
  _path: null,
  _polygons: [],
  inited: false,

  init(element) {
    if (this.inited) return;
    this._draw = SVG().addTo(element).size('100%', '100%');
    this.inited = true;
  },

  _getPoints(element) {
    return {
      top: element[0].offsetTop - 3,
      left: element[0].offsetLeft - 3,
      right: element[0].offsetLeft + element[0].offsetWidth + 3,
      bottom: element[0].offsetTop + element[0].offsetHeight + 3,
    }
  },

  _getEdges(element) {
    const points = this._getPoints(element);
    return {
      top: [{
        x: points.left,
        y: points.top
      }, {
        x: points.right,
        y: points.top
      }],
      right: [{
        x: points.right,
        y: points.top
      }, {
        x: points.right,
        y: points.bottom
      }],
      bottom: [{
        x: points.right,
        y: points.bottom
      }, {
        x: points.left,
        y: points.bottom
      }],
      left: [{
        x: points.left,
        y: points.bottom
      }, {
        x: points.left,
        y: points.top
      }],
    }
  },

  drawBorders(grid, refs, myBets) {
    const field = {};
    const horizontalRows = {};
    const verticalRows = {};
    const myCells = myBets.map(item => item.number);
    grid.forEach((cell, cellIdx) => {
      if (typeof cell.key === 'string' && cell.amount && myCells.includes(cell.key)) {
        const currentCell = refs[`cell${cell.key}`];
        if (!currentCell) return;
        field[cell.key] = {
          count: 1,
          cell: currentCell[0],
          cellKey: cell.key,
          points: this._getPoints(currentCell),
          edges: this._getEdges(currentCell),
          links: (function () {
            if (['x', 'y', 'z'].includes(cell.key)) {
              switch (cell.key) {
                case 'x':
                  return {
                    top: null,
                    right: 1,
                    bottom: 'y',
                    left: null
                  };
                case 'y':
                  return {
                    top: 'x',
                    right: 6,
                    bottom: 'z',
                    left: null
                  };
                case 'z':
                  return {
                    top: 'y',
                    right: 11,
                    bottom: 0,
                    left: null
                  };
              }
            } else {
              switch (cell.key) {
                case 'a':
                  return {
                    top: 11,
                    right: 'b',
                    bottom: null,
                    left: 0
                  };
                case 'b':
                  return {
                    top: 12,
                    right: 'c',
                    bottom: null,
                    left: 'a'
                  };
                case 'c':
                  return {
                    top: 13,
                    right: 'd',
                    bottom: null,
                    left: 'b'
                  };
                case 'd':
                  return {
                    top: 14,
                    right: 'e',
                    bottom: null,
                    left: 'c'
                  };
                case 'e':
                  return {
                    top: 15,
                    right: null,
                    bottom: null,
                    left: 'd'
                  };
              }
            }
          })()
        };
        cell.connected.forEach((item, connectedId) => {
          if (!field[item]) {
            field[item] = {
              count: 1,
              cell: refs[`cell${item}`][0],
              cellKey: item,
              points: this._getPoints(refs[`cell${item}`]),
              edges: this._getEdges(refs[`cell${item}`]),
              links: {
                top: item - 5 <= 0 ? null : item - 5,
                left: (function () {
                  if (item === 1) return 'x';
                  if (item === 6) return 'y';
                  if (item === 11) return 'z';
                  return item - 1;
                })(),
                right: (function () {
                  if (['x', 'y', 'z'].includes(cell.key)) {
                    if (connectedId === cell.connected.length - 1) return null;
                    return item + 1;
                  }
                  if (['a', 'b', 'c', 'd', 'e'].includes(cell.key)) {
                    if (cell.key === 'e') return null;
                    return item + 1;
                  }
                })(),
                bottom: (function () {
                  if ((cell.key === 'z') || ['a', 'b', 'c', 'd', 'e'].includes(cell.key) && connectedId === 0) {
                    switch (item) {
                      case 11:
                        return 'a';
                      case 12:
                        return 'b';
                      case 13:
                        return 'c';
                      case 14:
                        return 'd';
                      case 15:
                        return 'e';
                    }
                  } else {
                    return item + 5;
                  }
                })()
              }
            }
          } else {
            field[item].count += 1;
          }
        });

        if (cell.key === 'x' || cell.key === 'y' || cell.key === 'z') {
          horizontalRows[cell.key] = {
            weight: grid.length + 1 - cellIdx,
            cell: currentCell[0],
            edges: this._getEdges(currentCell),
            connected: cell.connected.map((item, idx) => {
              return {
                weight: cell.connected.length - idx,
                cell: refs[`cell${item}`][0],
                edges: this._getEdges(refs[`cell${item}`])
              }
            })
          };
        } else {
          verticalRows[cell.key] = {
            weight: grid.length + 1 - cellIdx,
            cell: currentCell[0],
            edges: this._getEdges(currentCell),
            connected: cell.connected.map((item, idx) => {
              return {
                weight: cell.connected.length - idx,
                cell: refs[`cell${item}`][0],
                edges: this._getEdges(refs[`cell${item}`])
              }
            })
          };
        }
      }
    });

    this.field = field;
    const horizontalRowsKeys = Object.keys(horizontalRows);
    const verticalRowsKeys = Object.keys(verticalRows);


    let lastCell = {};

    //Can be removing after checking and approving
    let direction = 'right';

    const makePolygons = (startCell, defaultLast = 'left') => {
      if (!startCell) return [];
      const points = [];
      const run = (currentKey) => {
        if (!currentKey) return;
        const cell = field[currentKey];
        const right = canMoveRight(cell);
        const top = canMoveTop(cell);
        const bottom = canMoveBottom(cell);
        const left = canMoveLeft(cell);
        let last = getLastPoint(cell, lastCell);


        if (points.length > 3 && typeof cell.cellKey === 'string' && cell.cellKey === startCell.cellKey) {
          if (last === 'right') {
            points.push(getBottomRightPoint(cell));
            points.push(getBottomLeftPoint(cell));
          }
          return true;
        }

        if (!last) last = defaultLast;
        if (last === 'left') { //last ==== LEFT
          if (!top && right && bottom && left) { //Simple step to right
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
          if (!top && !right && bottom && left) { //Rotate from left to bottom
            points.push(getTopLeftPoint(cell));
            points.push(getTopRightPoint(cell));
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey);
          }
          if (!top && !right && !bottom && left) { //Turn back
            points.push(getTopLeftPoint(cell));
            points.push(getTopRightPoint(cell));
            points.push(getBottomRightPoint(cell));
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey);
          }
          if (!top && right && bottom && !left) {
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
          if (!top && right && !bottom && !left) {
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
          if (!top && right && !bottom && left) {
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
          if (top && !right && bottom && left) { //Rotate from left to top
            //No points need
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey);
          }
          if (top && right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey);
          }
        } else if (last === 'bottom') { //last === BOTTOM
          if (!top && !right && bottom && !left) { //Turn back
            points.push(getBottomLeftPoint(cell));
            points.push(getTopLeftPoint(cell));
            points.push(getTopRightPoint(cell));
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey);
          }
          if (!top && !right && bottom && left) { //Rotate from bottom to left
            //No points need
            lastCell = cell;
            direction = 'right';
            return run(left.cellKey);
          }
          if (!top && right && bottom && !left) { //Rotate from bottom to right
            points.push(getBottomLeftPoint(cell));
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
          if (!top && right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey);
          }
          if (top && right && bottom && !left) {
            points.push(getBottomLeftPoint(cell));
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey);
          }
          if (top && !right && bottom && !left) {
            points.push(getBottomLeftPoint(cell));
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey);
          }
          if (top && !right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey);
          }
          if (top && right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey);
          }
        } else if (last === 'top') { //last === TOP
          if (top && !right && bottom && left) { //Simple step to bottom
            points.push(getTopRightPoint(cell));
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey);
          }
          if (top && !right && !bottom && left) { //Rotate from top to left
            points.push(getTopRightPoint(cell));
            points.push(getBottomRightPoint(cell));
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey);
          }
          if (top && !right && bottom && !left) {
            points.push(getTopRightPoint(cell));
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey);
          }
          if (top && !right && !bottom && !left) { //Turn back
            points.push(getTopRightPoint(cell));
            points.push(getBottomRightPoint(cell));
            points.push(getBottomLeftPoint(cell));
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey);
          }
          if (top && right && bottom && left) {
            //No points need?
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey);
          }
        } else if (last === 'right') { //last === RIGHT
          if (!top && right && !bottom && left) {
            points.push(getBottomRightPoint(cell));
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey)
          }
          if (!top && right && !bottom && !left) { //Turn back
            points.push(getBottomRightPoint(cell));
            points.push(getBottomLeftPoint(cell));
            points.push(getTopLeftPoint(cell));
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey)
          }
          if (!top && right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey)
          }
          if (!top && right && !bottom && !left) {
            lastCell = cell;
            direction = 'right';
            return run(right.cellKey)
          }
          if (top && right && !bottom && left) {
            points.push(getBottomRightPoint(cell));
            lastCell = cell;
            direction = 'left';
            return run(left.cellKey)
          }
          if (top && right && !bottom && !left) { //Rotate from right to top
            points.push(getBottomRightPoint(cell));
            points.push(getBottomLeftPoint(cell));
            lastCell = cell;
            direction = 'top';
            return run(top.cellKey)
          }
          if (top && right && bottom && left) {
            //No points need
            lastCell = cell;
            direction = 'bottom';
            return run(bottom.cellKey)
          }
        }
      };
      run(startCell.cellKey);
      return points;
    };

    //const points = makePolygons();
    const polygonsArray = [];
    let doubled = false;

    if (horizontalRowsKeys.length && !verticalRowsKeys.length) {
      doubled = false;
      const splitedPolygons = [];
      horizontalRowsKeys.forEach(key => {
        splitedPolygons.push(field[key]);
      });
      splitedPolygons.forEach(item => {
        polygonsArray.push(makePolygons(item, 'left'));
      });
    } else if (!horizontalRowsKeys.length && verticalRowsKeys.length) {
      doubled = false;
      const splitedPolygons = [];
      verticalRowsKeys.forEach(key => {
        if (!field[field[key].links.right]) {
          splitedPolygons.push(field[key]);
        }
      });
      splitedPolygons.forEach(item => {
        polygonsArray.push(makePolygons(item, 'top'));
      });
    } else {
      doubled = true;
      polygonsArray.push(makePolygons(field[horizontalRowsKeys[0]], 'left'));
    }

    if (!this._draw) return;
    if (doubled) {
      this._polygons.forEach(pol => {
        pol.clear();
        pol.remove();
      });
      this._polygons.length = 0;
      let path = '';
      const points = polygonsArray[0];
      points.forEach((point, key) => {
        if (key === 0) {
          path += `M${point.x} ${point.y} `;
        } else {
          path += `L${point.x} ${point.y} `;
        }
        if (key === points.length - 1) path += `Z`;
      });
      if (this._path) {
        this._path.clear();
        this._path.plot(roundPathCorners(path, 10, false))
      } else {
        this._path = this._draw.path(roundPathCorners(path, 10, false)).fill('none').stroke({
          width: 2,
          color: this._settings.color,
        });
      }

    } else {
      this._polygons.forEach(pol => {
        pol.clear();
        pol.remove();
      });
      this._polygons.length = 0;
      polygonsArray.forEach((points, idx) => {
        let path = '';
        points.forEach((point, key) => {
          if (key === 0) {
            path += `M${point.x} ${point.y} `;
          } else {
            path += `L${point.x} ${point.y} `;
          }
          if (key === points.length - 1) path += `Z`;
        });
        this._polygons.push(this._draw.path(roundPathCorners(path, 10, false)).fill('none').stroke({
          width: 2,
          color: this._settings.color,
        }));
        /*        if (this._polygons[idx]) {
				  this._polygons[idx].clear();
				  this._polygons[idx].plot(roundPathCorners(path, 10, false))
				} else {
				  this._polygons.push(this._draw.path(roundPathCorners(path, 10, false)).fill('none').stroke({
					width: 2,
					color: this._settings.color,
				  }));
				}*/
      });
    }
  }
}

export default fieldAnimations;
