import Color from 'color';

const colorMixer = {
  maxBet: 10,
  background: {
    startColor: 'rgb(22, 45, 76)',
    finishColor: 'rgb(24, 32, 47)',
    background: null,
    rStep: null,
    gStep: null,
    bStep: null,
  },
  fontColor: {
    color: 'rgba(255, 255, 255, 1)',
    step: null
  },
  myBorder: {
    color: 'rgba(0, 186, 195, 1)',
    step: null
  },
  myShadow: {
    default: '0px 0px 4.5px 0px',
    color: 'rgba(0, 186, 195, 1)',
    step: null
  },
  border: {
    color: 'rgba(63, 71, 90, 1)',
    step: null
  },
  shadow: {
    default: '2.5px 4.3px 9px 0px',
    color: 'rgba(20, 20, 29, 1)',
    step: null
  },
  innerBorder: {
    color: 'rgba(243, 175, 17, 1)',
    step: null
  },
  innerShadow: {
    default: '0px 0px 4px',
    color: 'rgba(218, 167, 30, 1)',
    step: null
  },
  gradient: {
    start: 'rgba(243, 175, 17, 1)',
    finish: 'rgba(236, 112, 38, 1)',
    step: null
  },
  changeGradient(currentAmount, type = 'gradient', maxBet = this.maxBet) {
    if (!this[type].step) this[type].step = 1 / maxBet;
    const colorStart = Color(this[type].start);
    const colorEnd = Color(this[type].finish);
    const alpha = 1;
    return 'linear-gradient(90deg, ' + colorStart.alpha(alpha).rgb().toString() + ', ' + colorEnd.alpha(alpha).rgb().toString() + ');';
  },
  changeBackgroundColor(currentAmount, maxBet = this.maxBet) {
    const startColor = Color(this.background.startColor).object();
    const finishColor = Color(this.background.finishColor).object();
      const rStep = (finishColor.r - startColor.r) / maxBet;
      const gStep = (finishColor.g - startColor.g) / maxBet;
      const bStep = (finishColor.b - startColor.b) / maxBet;
    return Color.rgb(startColor.r + rStep * currentAmount, startColor.g + gStep * currentAmount, startColor.b + bStep * currentAmount).rgb().toString();
  },
  changeShadow(currentAmount, type = 'shadow', maxBet = this.maxBet) {
    if (!this[type].step) this[type].step = 1 / maxBet;
    const color = Color(this[type].color);
    return color.alpha(1 - this[type].step * currentAmount).rgb().toString();
  },
  changeBorder(currentAmount, type = 'border', maxBet = this.maxBet) {
    if (!this[type].step) this[type].step = 1 / maxBet;
    const color = Color(this[type].color);
    return color.alpha(1 - this[type].step * currentAmount).rgb().toString();
  },
  changeColor(currentAmount, type = 'color', maxBet = this.maxBet) {
    if (!this[type].step) this[type].step = 1 / maxBet;
    const color = Color(this[type].color);
    let alpha = 1 - this[type].step * currentAmount;
    if (alpha < .1) {
      alpha = .1;
    }
    return color.alpha(alpha).rgb().toString();
  },
};

export default colorMixer;
