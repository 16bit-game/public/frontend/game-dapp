import {SVG} from '@svgdotjs/svg.js'

class SvgGrid {
  constructor(element) {
    this._draw = SVG().addTo(element).size('100%', '100%');
    this._settings = {
      color: 'rgb(30, 41, 60)',
      //color: 'rgb(255, 0, 0)',
      padding: 8,
      width: 1
    }
  }

  _drawHorizontalLine(offset) {
    this._draw.line(0, offset, '100%', offset).stroke({
      width: this._settings.width,
      color: this._settings.color,
    });
  }

  _drawVerticalLine(offset) {
    this._draw.line(offset, 0, offset, '100%').stroke({
      width: this._settings.width,
      color: this._settings.color,
    });
  }

  _formatGrid() {
    const cells = document.querySelectorAll('.playTable_body-inner .cell');
    const rows = [];
    let row = [];
    cells.forEach((item, index) => {
      if (index % 6 === 0) {
        row = [];
        rows.push(row);
        row.push(item);
      } else {
        row.push(item);
      }
    });
    this._drawGrid(rows);
  }

  _drawGrid(rows = []) {
    rows.forEach((row, index) => {
      if (index === 0) {
        const cells = row;
        const offsetX = 10;
        const offsetY = 11;
        this._drawVerticalLine(offsetX);
        this._drawHorizontalLine(offsetY);
        cells.forEach((cell, cidx) => {
          let correct = 0;
          switch (cidx) {
            case 0: {
              correct = -1.5;
              break;
            }
            case 1: {
              correct = -1;
              break;
            }
            case 2: {
              correct = -1.5;
              break;
            }
            case 3: {
              correct = -1;
              break;
            }
            case 4: {
              correct = -1.5;
              break;
            }
            case 5: {
              correct = -1;
              break;
            }
          }
          const offsetX = cell.offsetLeft + cell.offsetWidth + correct;
          this._drawVerticalLine(offsetX);
        });
      } else {
        const offsetY = row[0].offsetTop - 1;
        this._drawHorizontalLine(offsetY);
        if (index === rows.length - 1) {
          this._drawHorizontalLine(offsetY + row[0].offsetHeight);
        }
      }
    });
  }

  init() {
    this._formatGrid();
  }
}

export default SvgGrid;
