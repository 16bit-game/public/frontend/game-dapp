const { PowerUpState, Resources } = require("@greymass/eosio-resources");
const resources_eos = new Resources({
  url: "https://api.main.alohaeos.com",
});


const resourceManager = {
    sample: {},
    powerup: {},
    async refreshCache() {
        this.sample = await resources_eos.getSampledUsage();
        this.powerup = await resources_eos.v1.powerup.get_state();
    },
    init() {
        this.refreshCache();
    },
    price_per_us(usage, us = 1000, options) {
        const frac = this.powerup.cpu.frac(usage, us);
        const utilization_increase = this.powerup.cpu.utilization_increase(
          usage.cpu,
          frac
        );
        const adjusted_utilization = this.powerup.cpu.determine_adjusted_utilization(
          options
        );
        const fee = this.powerup.cpu.fee(utilization_increase, adjusted_utilization);
        const precision = Math.pow(10, 4);
        const value = Math.ceil(fee * precision) / precision;
        return { value, frac };
    },
    price_per_byte(usage, bytes = 1000, options) {
        const frac = this.powerup.net.frac(usage, bytes);
        const utilization_increase = this.powerup.net.utilization_increase(
            usage.net,
            frac
        );
        const adjusted_utilization = this.powerup.net.determine_adjusted_utilization(
            options
        );
        const fee = this.powerup.net.fee(utilization_increase, adjusted_utilization);
        const precision = Math.pow(10, 4);
        const value = Math.ceil(fee * precision) / precision;
        return {value, frac};
    },
    calcNet(kb) {
        const cast = this.powerup.net.cast();
        const { value: price, frac } = this.price_per_byte(this.sample, kb * 1024);
        const recievedAmount =
            (frac * cast.weight) / this.powerup.net.initial_weight_ratio.toNumber();
        const utilization_increase = this.powerup.net.utilization_increase(
            this.sample.net,
            frac
        );
        const adjusted_utilization = this.powerup.net.determine_adjusted_utilization();
        const fee = this.powerup.net.fee(utilization_increase, adjusted_utilization);
        const precision = Math.pow(10, 4);
        const feeValue = Math.round(fee * precision) / precision;
        return { recievedAmount, price, feeValue, frac };
    },
    calcCpu(ms) {
        const cast = this.powerup.cpu.cast();
        const { value: price, frac } = this.price_per_us(this.sample, ms * 1000);
        const recievedAmount =
            (frac * cast.weight) / this.powerup.cpu.initial_weight_ratio.toNumber();
        const utilization_increase = this.powerup.cpu.utilization_increase(
            this.sample.cpu,
            frac
        );
        const adjusted_utilization = this.powerup.cpu.determine_adjusted_utilization();
        const fee = this.powerup.cpu.fee(utilization_increase, adjusted_utilization);
        const precision = Math.pow(10, 4);
        const feeValue = Math.round(fee * precision) / precision;
        return { recievedAmount, price, feeValue, frac };
    }
}

export default resourceManager;
