import notification
  from '~/components/UI/Notification/main.js'

import config from '~/config/';
import socketCluster from 'socketcluster-client';
import scCodecMinBin from 'sc-codec-min-bin';


class Ws {
  constructor(network = 'testnet') {
    this.activeSocket = {};
    this._testnet = socketCluster.connect({
      path: '/testnet/v1/',
      hostname: config.WS,
      secure: true,
      port: 443,
      autoConnect: false,
      autoReconnectOptions: {
        initialDelay: 3000,
      },
      codecEngine: scCodecMinBin
    });
    this._mainnet = socketCluster.connect({
      path: '/mainnet/v1/',
      hostname: config.WS,
      secure: true,
      port: 443,
      autoConnect: false,
      autoReconnectOptions: {
        initialDelay: 3000,
      },
      codecEngine: scCodecMinBin
    });
    this._cacheListeners = [];
    this.activeSocket = this['_' + network];
    this.activeSocket.connect();
  }

  changeNetwork(network) {
    if (!network) return;
    if (this.activeSocket) {
      this.activeSocket.removeAllListeners && this.activeSocket.removeAllListeners();
      this.activeSocket.disconnect && this.activeSocket.disconnect();
    }
    this.activeSocket = this['_' + network];
    this.activeSocket.connect();
    this._restoreListeners();
  }

  _restoreListeners() {
    this._cacheListeners.forEach(item => {
      this.on(item.evt, item.cb, false);
    })
  }

  on(evt, cb, add = true) {
    add && this._cacheListeners.push({
      evt,
      cb
    });
    this.activeSocket.on(evt, cb);
  }

  send(evt) {
    this.activeSocket.send(evt);
  }
}

const ws = new Ws('testnet');

export default ws;
