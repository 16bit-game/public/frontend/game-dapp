import to from 'await-to-js';
import ScatterJS from "@scatterjs/core/dist/index";
import {Api, JsonRpc} from 'eosjs';
import ScatterEOS from '@scatterjs/eosjs2';
import ScatterLynx from '@scatterjs/lynx';
import config from '~/config';

ScatterJS.plugins(new ScatterEOS(), new ScatterLynx({
  Api,
  JsonRpc
}));

const Scatter = {
  connected: false,
  api: null,
  checked: false,
  instance: null,

  network: {
    current: null,
    testnet: {
      name: '16Bit',
      blockchain: 'eos',
      chainId: '5fff1dae8dc8e2fc4d5b23b2c7665c97f9e9d8edf2b6485a86ba311c25639191',
      host: config.EOS.NODE['testnet'].replace(/(^(http|https):\/\/)|(\:\d*)/gi, ''),
      port: 443,
      protocol: 'https'
    },
    mainnet: {
      name: '16Bit',
      blockchain: 'eos',
      chainId: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
      host: config.EOS.NODE['mainnet'].replace(/(^(http|https):\/\/)|(\:\d*)/gi, ''),
      port: 443,
      protocol: 'https'
    }
  },
  //Changing current scatter network
  //TODO remove this comment - step 1;
  makeScatterNetwork(network = 'mainnet') {
    this.network.current = ScatterJS.Network.fromJson(this.network[network]);
  },

  //Checking Scatter
  //TODO remove this comment - step 2;
  async init() {
    const [err, connected] = await to(ScatterJS.connect('16Bit', {network: this.network.current}));
    if (err || !connected) throw (err || 'Scatter not found on your device');
    this.connected = connected;
    this.api = ScatterJS.eos(this.network.current, Api, {rpc: new JsonRpc(this.network.current.fullhost())});
    this.instance = ScatterJS.scatter;
    this.instance.suggestNetwork(this.network.current);
    return connected;
  },

  getEosAccount() {
    if (ScatterJS.scatter.identity) {
      return ScatterJS.account('eos');
    } else {
      return false
    }
  },

  async login() {
    if (!this.connected) {
      let [err] = await to(this.init());
      if (err) {
        throw {
          message: err
        };
      }
    }
    const [err] = await to(ScatterJS.login());
    if (err) throw err;
    return ScatterJS.account('eos');
  },

  logout() {
    if (this.instance) {
      this.instance.forgetIdentity();
      this.instance.logout();
    }
  }
};

export default Scatter;
