import * as bip39 from 'bip39';
import hdKey from 'hdkey';
import ecc from 'eosjs-ecc';
import wif from 'wif';


const Wallet = {
  ownerPublicKey: '',
  ownerPrivateKey: '',
  activePublicKey: '',
  activePrivateKey: '',
  generateMnemonic() {
    this.mnemonic = bip39.generateMnemonic(256);
    return this.mnemonic;
  },
  async getKeys(mnemonic = this.mnemonic) {
    if (!mnemonic) reject('Mnemonic is empty');
    const res = await bip39.mnemonicToSeed(mnemonic);
    const hex = res.toString('hex');
    const master = hdKey.fromMasterSeed(hex);
    const node = master.derive("m/44'/194'/0'/0/0");
    const ownerPublicKey = ecc.PublicKey(node._publicKey).toString();
    const ownerPrivateKey = wif.encode(128, node._privateKey, false);
    this.ownerPublicKey = ownerPublicKey;
    this.ownerPrivateKey = ownerPrivateKey;
    const {activePublicKey, activePrivateKey} = this.activesFromOwner(ownerPrivateKey);
    this.activePublicKey = activePublicKey;
    this.activePrivateKey = activePrivateKey;
    //TODO check where using
    return {
      publicKey: `${this.activePublicKey}:${this.ownerPublicKey}`,
      privateKey: `${this.activePrivateKey}:${this.ownerPrivateKey}`
    }
  },
  activesFromOwner(key) {
    const eccPrivate = ecc.PrivateKey(key).getChildKey('owner').getChildKey('active');
    return {
      activePublicKey: eccPrivate.toPublic().toString(),
      activePrivateKey: eccPrivate.toString()
    }
  },
  privateToMnemonic(key) {
    const prk = wif.decode(key);
    return bip39.entropyToMnemonic(prk.privateKey);
  },
  privateToPublic(key, prefix = 'EOS') {
    if (typeof key === 'string') return [ecc.privateToPublic(key, prefix)];
    const keys = [ecc.privateToPublic(key[0], prefix)];
    if (key.length > 1) {
      keys.push(ecc.privateToPublic(key[1], prefix));
    }
    return keys;
  },
  isValidPrivate(key) {
    if (typeof key === 'string') return ecc.isValidPrivate(key);
    let validCount = 0;
    if (ecc.isValidPrivate(key[0])) validCount++;
    if (key.length > 1) {
      if (ecc.isValidPrivate(key[1])) validCount++;
    }
    return key.length === validCount;
  },
  getKeysFromPermissions(permissions) {
    const keys = {
      active: '',
      owner: ''
    };
    permissions.forEach(item => {
      if (item.perm_name === 'active' && item.parent === 'owner') keys.active = item.required_auth.keys[0].key;
      if (item.perm_name === 'owner' && item.parent === '') keys.owner = item.required_auth.keys[0].key;
    });
    this.ownerPublicKey = keys.owner;
    this.activePublicKey = keys.active;
    return keys;
  }
};

export {Wallet};
