import ScatterJS from '@scatterjs/core';
import {Eos} from "~/services/provider/eos";
import {Api, JsonRpc} from 'eosjs';
import ScatterEOS from '@scatterjs/eosjs2';
import ScatterLynx from '@scatterjs/lynx';
import config from '~/config';





const ScatterWallet = {
  connected: false,
  checked: false,
  scatter: ScatterJS.scatter,
  network: {
    scatter: null,
    testnet: {
      name: '16Bit',
      blockchain: 'eos',
      chainId: '5fff1dae8dc8e2fc4d5b23b2c7665c97f9e9d8edf2b6485a86ba311c25639191',
      host: config.EOS.NODE['testnet'].replace(/(^(http|https):\/\/)|(\:\d*)/gi, ''),
      port: 443,
      protocol: 'https'
    },
    mainnet: {
      name: '16Bit',
      blockchain: 'eos',
      chainId: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
      host: config.EOS.NODE['mainnet'].replace(/(^(http|https):\/\/)|(\:\d*)/gi, ''),
      port: 443,
      protocol: 'https'
    }
  },
  makeScatterNetwork(network = 'testnet') {
    this.network.scatter = ScatterJS.Network.fromJson(this.network[network]);
  },
  connect() {
    return new Promise((resolve, reject) => {
      ScatterJS.scatter.connect("16Bit", {network: this.network.scatter}).then(connected => {
        this.connected = connected;
        if (ScatterJS.scatter.identity) {
          const account = ScatterJS.scatter.account('eos');
          resolve({
            connected: true,
            account
          });
        } else {
          resolve({
            connected: true,
            account: false
          });
        }
      }).catch(e => {
        this.connected = false;
        console.log('scatter connect error - ', e);
        reject(e);
      });
    })
  },
  login() {
    return new Promise(async (resolve, reject) => {
      try {
        //await this.connect();
        if (!this.connected) await this.connect();
        if (!ScatterJS.scatter.login) return reject();
        const identity = await ScatterJS.scatter.getIdentity({accounts: [this.network.scatter]});
        ScatterJS.scatter.login(/*{
          accounts: [this.network.testnet, this.network.mainnet]
        }*/).then(res => {
          console.log('ScatterWallet.js login success!', res);
          Eos.initScatterApi();
          return resolve(res);
        }).catch(err => {
          console.log('ScatterWallet.js login - normal error!', err);
          return reject(err);
        });
      } catch (e) {
        console.log('ScatterWallet.js login error', e);
        return reject(e);
      }
    })
  },
  init() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.connect();
        //console.log('scatter init!', res, this.connected, this.checked);
        if (res.account) {
          Eos.initScatterApi();
        } else {
          !this.network.scatter && this.makeScatterNetwork();
          Eos.init();
        }
        this.checked = true;
        return resolve(res);
      } catch (e) {
        console.log('scatter init ERROR!', e);
        this.checked = true;
        return reject(e);
      }
    });
  }
};

export default ScatterWallet;
