const toLowerEndian = (t) => {
  const fw = t.slice(0, t.length / 2).match(/.{0,2}/gi);
  const fw2 = t.slice(t.length / 2, t.length).match(/.{0,2}/gi);
  const pfw = [];
  const pfw2 = [];
  for (let i = 0; i < 16; i++) {
    pfw[15 - i] = fw[i];
    pfw2[15 - i] = fw2[i];
  }
  return pfw.join('') + pfw2.join('')
};
import {JsSignatureProvider} from 'eosjs/dist/eosjs-jssig';
import {Api, JsonRpc} from 'eosjs';
import {
  sha256
} from 'eosjs-ecc';
import {getFingerPrint} from '@/components/utils/';

const demoBet = async ({key, fullHost, contract, actionsData} = {}) => {
  const signatureProvider = new JsSignatureProvider([key]);
  const api = new Api({
    rpc: new JsonRpc(fullHost),
    signatureProvider
  });

  const token = await api.getCachedAbi('eosio.token');
  token.abi.structs.find(action => {
    action.name == 'transfer' && action.fields.push({name: 'target', type: 'name'});
  });
  token.rawAbi = api.jsonToRawAbi(token.abi);
  api.cachedAbis.set('eosio.token', token);

  return api.transact({
    actions: [{
      account: contract,
      name: 'bet',
      data: actionsData,
      authorization: [{
        actor: actionsData.player,
        permission: 'active'
      }]
    }, {
      account: 'eosio.token',
      name: 'transfer',
      data: {
        from: actionsData.player,
        to: 'fairbet.bank',
        quantity: actionsData.bet.quantity,
        memo: toLowerEndian(actionsData.bet.seed),
        target: 'fairbet.game'
      },
      authorization: [
        {
          actor: actionsData.player,
          permission: "active"
        }
      ]
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).then(res => {
    postMessage({
      type: 'success',
      payload: res
    })
  }).catch(e => {
    postMessage({
      type: 'betTransactionError',
      payload: e
    })
  });
};

const bet = async ({key, fullhost, contract, accountName, game, amount, numbers, serverTransaction, isDemo} = {}) => {
  const currentSeed = await getFingerPrint();
  const seed = sha256(currentSeed);
  const signatureProvider = new JsSignatureProvider([key]);
  const api = new Api({
    rpc: new JsonRpc(fullhost),
    signatureProvider
  });
  let data = {
    player: accountName,
    bet: {
      game: game,
      seed: seed,
      quantity: amount,
      numbers: numbers
    }
  };
  if (serverTransaction.affiliate) data.affiliate = serverTransaction.affiliate;
  return api.transact({
    actions: [{
      account: contract,
      name: 'bet',
      data: data,
      authorization: [{
        actor: accountName,
        permission: 'active'
      }]
    }, {
      account: 'eosio.token',
      name: 'transfer',
      data: {
        from: accountName,
        to: isDemo ? 'fairbet.bank' : contract,
        quantity: amount,
        memo: toLowerEndian(seed)
      },
      authorization: [
        {
          actor: accountName,
          permission: "active"
        }
      ]
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).then(res => {
    postMessage({
      type: 'success',
      payload: res
    })
  }).catch(e => {
    postMessage({
      type: 'betTransactionError',
      payload: e
    })
  });
};

const payCpuBet = async ({tx, serverTransaction, pushTransactionArgs, key, fullhost}) => {
  const signatureProvider = new JsSignatureProvider([key]);
  const api = new Api({
    rpc: new JsonRpc(fullhost),
    signatureProvider
  });

  await api.transact(tx, {
    blocksBehind: 3,
    expireSeconds: 60,
    sign: false,
    broadcast: false
  });
  pushTransactionArgs.signatures.unshift(
    serverTransaction.signatures[0]
  );
  const d = await api.pushSignedTransaction(pushTransactionArgs);
  postMessage({
    type: 'success',
    payload: JSON.stringify(d)
  });
};

const createSignatures = async ({tx, key, fullhost}) => {
  const signatureProvider = new JsSignatureProvider([key]);
  const api = new Api({
    rpc: new JsonRpc(fullhost),
    signatureProvider
  });
  const pushTransactionArgs = await api.transact(tx, {
    blocksBehind: 3,
    expireSeconds: 60,
    sign: false,
    broadcast: false
  });
  const requiredKeys = await api.signatureProvider.getAvailableKeys();
  const serializedTx = Buffer.from(pushTransactionArgs.serializedTransaction, 'hex');
  const signArgs = {
    chainId: api.chainId,
    requiredKeys,
    serializedTransaction: serializedTx,
    abis: [],
  };
  const s = await api.signatureProvider.sign(signArgs);
  postMessage({
    type: 'success',
    payload: s
  });
};


onmessage = (msg) => {
  if (msg.data.tx && !msg.data.serverTransaction) {
    createSignatures(msg.data);
  } else if (msg.data.tx && msg.data.serverTransaction) {
    payCpuBet(msg.data);
  } else if (msg.data.isDemo) {
    demoBet(msg.data);
  } else {
    bet(msg.data);
  }
};

export default {}
