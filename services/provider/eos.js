import ScatterJS from "@scatterjs/core/dist/index";
import {Api, JsonRpc, RpcError} from 'eosjs';
import {JsSignatureProvider} from 'eosjs/dist/eosjs-jssig';
import CreateBetTransaction
  from './createBetTransaction.worker';
import axios from 'axios';
import FetchBalancer
  from '~/components/utils/fetchBalancer';
import Scatter from '~/services/wallet/scatter';

import {convertLegacyPublicKey} from 'eosjs/dist/eosjs-numeric'
import {
  sha256,
} from 'eosjs-ecc';

import config from '~/config/';

import {ec} from 'elliptic';

import * as base58 from '~/components/utils/bs58.bundle.js';
import {getFingerPrint} from '@/components/utils/';

const eosNetwork = config.EOS.NODE['mainnet'].replace(/(^(http|https):\/\/)|(\:\d*)/gi, '');


const fetchMainnet = new FetchBalancer({
  endpoints: config.rpcNodes,
  upstreamOptions: {
    retry: [3]
  }
});

// TODO: refactor this damn
const _bet = async ({api, contract, accountName, game, amount, numbers}) => {
  const currentSeed = await getFingerPrint();
  const seed = sha256(currentSeed);
  return api.transact({
    actions: [{
      account: contract,
      name: 'bet',
      data: {
        player: accountName,
        bet: {
          game: game,
          seed: seed,
          quantity: amount,
          numbers: numbers
        }
      },
      authorization: [{
        actor: accountName,
        permission: 'active'
      }]
    }, {
      account: 'eosio.token',
      name: 'transfer',
      data: {
        from: accountName,
        to: contract,
        quantity: amount,
        memo: toLowerEndian(seed)
      },
      authorization: [
        {
          actor: accountName,
          permission: "active"
        }
      ]
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  });
};

const bet = async ({api, key, fullHost, contract, actionsData} = {}) => {
  const token = await api.getCachedAbi('eosio.token');
  token.abi.structs.find(action => {
    action.name == 'transfer' && action.fields.push({
      name: 'target',
      type: 'name'
    });
  });
  token.rawAbi = api.jsonToRawAbi(token.abi);
  api.cachedAbis.set('eosio.token', token);
  return api.transact({
    actions: [{
      account: contract,
      name: 'bet',
      data: actionsData,
      authorization: [{
        actor: actionsData.player,
        permission: 'active'
      }]
    }, {
      account: 'eosio.token',
      name: 'transfer',
      data: {
        from: actionsData.player,
        to: 'fairbet.bank',
        quantity: actionsData.bet.quantity,
        memo: toLowerEndian(actionsData.bet.seed),
      },
      authorization: [
        {
          actor: actionsData.player,
          permission: "active"
        }
      ]
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  });
};

const createSignatures = async ({api, tx, serverTransaction}) => {
  const token = await api.getCachedAbi('eosio.token');
  token.abi.structs.find(action => {
    action.name == 'transfer' && action.fields.push({
      name: 'target',
      type: 'name'
    });
  });
  token.rawAbi = api.jsonToRawAbi(token.abi);
  api.cachedAbis.set('eosio.token', token);
  await api.transact(tx, {
    blocksBehind: 3,
    expireSeconds: 60,
    sign: false,
    broadcast: false
  });
  const network = ScatterJS.Network.fromJson({
    blockchain: 'eos',
    chainId: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
    host: eosNetwork,
    port: 443,
    protocol: 'https'
  });
  let signatureProvider = {};
  const publicKey = (ScatterJS.scatter.identity && ScatterJS.scatter.account('eos').publicKey) || localStorage.getItem('public');
  let requiredKeys = null;
  signatureProvider = ScatterJS.scatter.eosHook({
    ...network,
    blockchain: 'eos'
  });
  if (!api.signatureProvider.getAvailableKeys) {
    requiredKeys = convertLegacyPublicKey(publicKey);

    /*    signatureProvider = ScatterJS.scatter.eosHook({
		  ...network,
		  blockchain: 'eos'
		});*/
  } else {
    requiredKeys = [convertLegacyPublicKey(publicKey)];

    //requiredKeys = await api.signatureProvider.getAvailableKeys();
  }


  const serializedTx = Buffer.from(serverTransaction.serializedTransaction, 'hex');

  const signArgs = {
    chainId: network.chainId,
    requiredKeys,
    serializedTransaction: serializedTx,
    abis: [],
  };


  /*  signatureProvider.sign(signArgs).then(res => {
	  console.log('AFTER SIGN', res);
	}).catch(e => {
	  console.log('AFTER SIGN', e);
	});*/

  const pushTransactionArgs = api.signatureProvider.sign ? await api.signatureProvider.sign(signArgs) : await signatureProvider.sign(signArgs);

  pushTransactionArgs.signatures.unshift(
    serverTransaction.signatures[1]
  );

  //const d = await api.pushSignedTransaction(pushTransactionArgs);
  const d = await api.pushCompressedSignedTransaction(pushTransactionArgs);
  return d;
};

const contract = config.EOS.CONTRACT;
const Eos = {
  api: null,
  privateKey: '',
  currentSeed: null,
  seedHash: null,
  async __test({key, fullhost, contract, accountName, game, amount, numbers, serverTransaction} = {}) {
    const t = `${bs58.decode('7ZVHJ1MAPGxJ5FZ4K8gDYoj9GFp8kevqhkKeVHv64LkhnzYBVS').toString('hex')}`;
    const currentSeed = await getFingerPrint();
    const seed = sha256(currentSeed);
    amount = parseFloat(amount).toFixed(4) + ' EOS';
    let data = {};
    let actions = [{
      account: contract,
      name: 'test',
      data: data,
      authorization: [{
        actor: accountName,
        permission: 'active'
      }]
    }];
    const signatureProvider = new JsSignatureProvider([key]);
    const api = new Api({
      rpc: new JsonRpc(fullhost),
      signatureProvider
    });

    const pub = (await this.getPublicKey()).data.rows[0].witness;


    /*    let pushTransactionArgs = await api.transact(tx, {
		  blocksBehind: 3,
		  expireSeconds: 60,
		  sign: false,
		  broadcast: false
		});*/
    //const serializedTx = pushTransactionArgs.serializedTransaction;
    const signArgs = {
      chainId: '',
      requiredKeys: [pub],
      serializedTransaction: JSON.stringify(data),
      abis: [],
    };

    const defaultEc = new ec('secp256k1');
    const digestFromSerializedData = ({
                                        chainId,
                                        serializedTransaction,
                                        serializedContextFreeData = false,
                                        e = defaultEc
                                      } = {}) => {

      const signBuf = Buffer.concat([
        Buffer.from(chainId, 'hex'),
        Buffer.from(serializedTransaction),
        Buffer.from(
          serializedContextFreeData ?
            new Uint8Array(e.hash().update(serializedContextFreeData).digest()) :
            new Uint8Array(32)
        ),
      ]);
      return e.hash().update(signBuf).digest('hex');
    };

    const pushTransactionArgs = await api.signatureProvider.sign(signArgs);

    data = {
      digest: digestFromSerializedData(signArgs),
      proof: pushTransactionArgs.signatures[0]
    };
    actions = [{
      account: 'fairbetagent',
      name: 'test',
      data: data,
      authorization: [{
        actor: accountName,
        permission: 'active'
      }]
    }];
    const res = await api.transact({
      actions: actions
    }, {
      blocksBehind: 3,
      expireSeconds: 60,
    });
    /*return api.transact({
      actions: [{
        account: contract,
        name: 'bet',
        data: data,
        authorization: [{
          actor: accountName,
          permission: 'active'
        }]
      }, {
        account: 'eosio.token',
        name: 'transfer',
        data: {
          from: accountName,
          to: 'fairbet.bank',//contract,
          quantity: amount,
          memo: toLowerEndian(seed)
        },
        authorization: [
          {
            actor: accountName,
            permission: "active"
          }
        ]
      }]
    }, {
      blocksBehind: 3,
      expireSeconds: 30,
    });*/
  },
  setPureApi(privateKey) {
    //TODO: May be need to refactor setting private key;
    this.privateKey = privateKey;
    this.api = new Api({
      rpc: new JsonRpc('', {
        fetch: fetchMainnet
      }),
      signatureProvider: new JsSignatureProvider([privateKey])
    });
  },
  setScatterApi() {
    this.api = Scatter.api;
    Scatter.instance.suggestNetwork(Scatter.network.current);
  },
  clearState() {
    this.privateKey = null;
    this.api = null;
  },

  /**
   * @deprecated
   * @param key
   */
  init(key = localStorage.getItem('private')) {
    if (key) this.setKey(key);
    if (!this.privateKey || !key) return;
    const signatureProvider = new JsSignatureProvider([this.privateKey || key]);
    this.api = new Api({
      rpc: new JsonRpc(ScatterWallet.network.scatter.fullhost()),
      signatureProvider
    });
  },
  /**
   * @deprecated
   */
  initScatterApi() {
    // TODO need to refactor
    if (!ScatterWallet.network.scatter) return;
    const network = ScatterJS.Network.fromJson(ScatterWallet.network.scatter);
    this.api = ScatterJS.eos(network, Api, {rpc: new JsonRpc(network.fullhost())});
    ScatterWallet.scatter.suggestNetwork(ScatterWallet.network.scatter);
  },
  /**
   * @deprecated
   * @param key
   */
  setKey(key = '') {
    this.privateKey = key;
  },

  /**
   * @deprecated
   */
  createHash() {
    this.currentSeed = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    this.seedHash = sha256(this.currentSeed);
  },

  createDemoBet(actionsData) {
    return new Promise((resolve, reject) => {
      const key = this.privateKey || localStorage.getItem('private');
      const worker = new CreateBetTransaction();
      if (!key) {
        bet({
          api: this.api,
          key,
          fullHost: Scatter.network.current.fullhost(),
          contract,
          actionsData
        }).then(res => {
          resolve(res);
        }).catch(e => {
          reject(e);
        });
      } else {
        worker.postMessage({
          isDemo: true,
          key,
          fullHost: Scatter.network.current.fullhost(),
          contract,
          actionsData
        });
        worker.onmessage = (msg) => {
          if (msg.data.type === 'success') {
            resolve(msg.data.payload);
          } else {
            reject(msg.data.payload);
          }
          worker.terminate();
        };
      }
    });
  },

  createBet({isDemo, tx, serverTransaction, pushTransactionArgs, accountName, numbers, amount, game, additionalResponse} = {}) {
    return new Promise((resolve, reject) => {
      amount = amount.toFixed(4) + ' EOS';
      const key = this.privateKey || localStorage.getItem('private');
      const worker = new CreateBetTransaction();
      if (!key || !isDemo) {
        if (tx && serverTransaction) {
          createSignatures({
            api: this.api,
            tx,
            serverTransaction
          }).then(res => {
            resolve(res);
          }).catch(e => {
            reject(e);
          });
        } else {
          bet({
            api: this.api,
            key,
            fullHost: Scatter.network.current.fullhost(),
            contract,
            actionsData: additionalResponse
          }).then(res => {
            resolve(res);
          }).catch(e => {
            reject(e);
          });
        }
      } else {
        worker.postMessage({
          tx,
          serverTransaction,
          pushTransactionArgs,
          key,
          fullhost: Scatter.network.current.fullhost(),
          contract,
          accountName,
          game,
          amount,
          numbers,
          isDemo
        });
        worker.onmessage = (msg) => {
          if (msg.data.type === 'success') {
            resolve(msg.data.payload);
          } else {
            reject(msg.data.payload);
          }
          worker.terminate();
        };
      }
    });
  },
  async createSignatures({tx, serverTransaction} = {}) {
    await this.api.transact(tx, {
      blocksBehind: 3,
      expireSeconds: 60,
      sign: false,
      broadcast: false
    });
    const requiredKeys = await this.api.signatureProvider.getAvailableKeys();
    const serializedTx = Buffer.from(serverTransaction.serializedTransaction, 'hex');
    const signArgs = {
      chainId: this.api.chainId,
      requiredKeys,
      serializedTransaction: serializedTx,
      abis: [],
    };
    let pushTransactionArgs = await this.api.signatureProvider.sign(signArgs);
    pushTransactionArgs.signatures.unshift(
      serverTransaction.signatures[0]
    );
    return this.api.pushSignedTransaction(pushTransactionArgs);
  },
  makeRamActions({accountName, amount = 1024 * 4} = {}) {
    return {
      actions: [{
        account: 'eosio',
        name: 'buyrambytes',
        data: {
          payer: accountName,
          receiver: accountName,
          bytes: amount,
        },
        authorization: [{
          actor: accountName,
          permission: 'active',
        }]
      }]
    }
  },
  //TODO refactor (replace to no uniq func)
  makePowerupAction({accountName, cpu_frac = 0, net_frac = 0, max_payment = 0} = {}) {
    max_payment = max_payment.toFixed(4) + ' EOS';
    return {
      actions: [{
        account: 'eosio',
        name: 'powerup',
        data: {
          payer: accountName,
          receiver: accountName,
          days: 1,
          net_frac: net_frac,
          cpu_frac: cpu_frac,
          max_payment: max_payment
        },
        authorization: [{
          actor: accountName,
          permission: 'active'
        }]
      }]
    }
  },
  makeNetActions({accountName, amount}) {
    amount = amount.toFixed(4) + ' EOS';
    return {
      actions: [{
        account: 'eosio',
        name: 'deposit',
        data: {
          owner: accountName,
          amount: amount
        },
        authorization: [{
          actor: accountName,
          permission: 'active'
        }]
      }, {
        account: 'eosio',
        name: 'rentnet',
        data: {
          from: accountName,
          loan_fund: '0.0000 EOS',
          loan_payment: amount,
          receiver: accountName
        },
        authorization: [{
          actor: accountName,
          permission: 'active'
        }]
      }]
    }
  },

  async makeActions({contract, accountName, game, amount, numbers, seed}) {
    amount = parseFloat(amount).toFixed(4) + ' EOS';
    return {
      actions: [{
        account: contract,
        name: 'bet',
        data: {
          player: accountName,
          bet: {
            game: game,
            seed: seed,
            quantity: amount,
            numbers: numbers
          }
        },
        authorization: [{
          actor: accountName,
          permission: 'active'
        }]
      }, {
        account: 'eosio.token',
        name: 'transfer',
        data: {
          from: accountName,
          to: 'fairbet.bank',
          quantity: amount,
          memo: toLowerEndian(seed),
          target: 'fairbet.game'
        },
        authorization: [
          {
            actor: accountName,
            permission: "active"
          }
        ]
      }]
    }
  },
  buyRam(accountname = localStorage.getItem('accountname')) {
    this.api.transact({
      actions: [{
        account: 'eosio',
        name: 'buyrambytes',
        data: {
          payer: localStorage.getItem('accountname'),
          receiver: localStorage.getItem('accountname'),
          bytes: 12313,
        },
        authorization: [{
          actor: localStorage.getItem('accountname'),
          permission: 'active',
        }]
      }]
    }, {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  },
  stackCpu() {
    this.api.transact({
      actions: [{
        account: 'eosio',
        name: 'delegatebw',
        authorization: [{
          actor: localStorage.getItem('accountname'),
          permission: 'active',
        }],
        data: {
          from: localStorage.getItem('accountname'),
          receiver: localStorage.getItem('accountname'),
          stake_net_quantity: '2.0000 EOS',
          stake_cpu_quantity: '2.0000 EOS',
          transfer: false
        },
      }]
    }, {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  },
  async transfer({from, to, amount, memo}) {
    return this.api.transact({
      actions: [{
        account: 'eosio.token',
        name: 'transfer',
        data: {
          from: from,
          to: to,
          quantity: amount,
          memo: memo
        },
        authorization: [{
          actor: from,
          permission: "active"
        }]
      }]
    }, {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  },
  async _fetchRows(options) {
    const mergedOptions = {
      json: true,
      limit: 9999,
      ...options,
    };
    const result = await this.api.rpc.get_table_rows(mergedOptions);
    return result.rows;
  },
  getPublicKey() {
    return axios.post(`${Scatter.network.current.protocol}://${Scatter.network.current.host}/v1/chain/get_table_rows`, {
      json: true,
      code: 'fairbetagent',
      scope: 'fairbetagent',
      table: 'state',
      lowerBound: 1,
      upperBound: 1
    });
  },
  getRexInfo() {
    return axios.post(`${config.EOS.NODE_UTILITY}/v1/chain/get_table_rows`, {
      json: true,
      code: 'eosio',
      scope: 'eosio',
      table: `rexpool`,
      index_position: 1,
      limit: 10,
    })
  },
  getRamPrice() {
    return new Promise((resolve, reject) => {
      axios.post(`${config.EOS.NODE_UTILITY}/v1/chain/get_table_rows`, {
        json: true,
        code: 'eosio',
        scope: 'eosio',
        table: 'rammarket'
      }).then(res => {
        resolve(res.data.rows)
      }).catch(e => {
        reject(e);
      })
    });
  }
};

const toLowerEndian = (t) => {
  const fw = t.slice(0, t.length / 2).match(/.{0,2}/gi);
  const fw2 = t.slice(t.length / 2, t.length).match(/.{0,2}/gi);
  const pfw = [];
  const pfw2 = [];
  for (let i = 0; i < 16; i++) {
    pfw[15 - i] = fw[i];
    pfw2[15 - i] = fw2[i];
  }
  return pfw.join('') + pfw2.join('')
};

export {Eos};
