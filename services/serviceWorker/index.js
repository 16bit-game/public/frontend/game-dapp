import {register} from 'register-service-worker'

export default ({app}) => {
  console.log('SERVICE WORKER - ', app);
  return register('./sw.js', {
    registrationOptions: {scope: './'},
    ready(event) {
      console.log('Service worker is active.', self, event)
    },
    registered(registration) {
      console.log('Service worker has been registered.')
      self.addEventListener('fetch', function fetcher(event) {
        const request = event.request;
        if (request.url.indexOf('changenow.io/images/sprite') > -1) {
          // contentful asset detected
          event.respondWith(
            caches.match(event.request).then(function (response) {
              // return from cache, otherwise fetch from network
              return response || fetch(request);
            })
          );
        }
      });
    },
    cached(registration) {
      console.log('Content has been cached for offline use.')
    },
    updatefound(registration) {
      console.log('New content is downloading.')
    },
    updated(registration) {
      console.log('New content is available; please refresh.')
    },
    offline() {
      console.log('No internet connection found. App is running in offline mode.')
    },
    error(error) {
      console.error('Error during service worker registration:', error)
    }
  });
}
