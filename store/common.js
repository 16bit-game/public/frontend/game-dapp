import req from '~/components/api/req';
import config from '~/config';
import {Eos} from "~/services/provider/eos";
import {Wallet} from '~/services/wallet/index';
import ws from '~/services/ws';
import Scatter from '~/services/wallet/scatter';
import to from 'await-to-js';


export const state = () => ({
  data: [],
  locale: localStorage.getItem('lang') || 'en-EN',
  showingModal: '',
  demo: (localStorage.getItem('demo') === 'true' ? true : false),
  useFreeCpu: false,
  network: '',
  showingMenu: false,
  waiting: false,
  clockTrouble: false,
  accountExist: false
});
export const mutations = {
  SET_DATA(store, array) {
    store.data = array;
  },
  SET_LANGUAGE(store, lang) {
    localStorage.setItem('lang', lang);
    store.locale = lang;
  },
  SET_ACCOUNT_EXISTING(store, boolean) {
    store.accountExist = boolean;
  },
  SET_SHOWING_MODAL(store, string) {
    store.showingModal = string;
  },
  SET_SHOW_MENU(store, boolean) {
    store.showingMenu = boolean;
  },
  SET_GAME_TYPE(store, boolean) {
    store.demo = boolean;
    store.network = boolean ? 'testnet' : 'mainnet';
    localStorage.setItem('demo', boolean);
  },
  SET_FIELD(store, {field, value} = {}) {
    store[field] = value;
  },
  SET_WAITING_STATE(store, boolean) {
    store.waiting = boolean;
  },
  SHOW_CLOCK_TROUBLE(store, boolean) {
    store.clockTrouble = boolean;
  }
};
export const actions = {
  waitingState(context, boolean) {
    context.commit('SET_WAITING_STATE', boolean);
  },
  clockTrouble(context, boolean) {
    context.commit('SHOW_CLOCK_TROUBLE', boolean);
  },
  fetch(context) {
  },
  /**
   *
   * @param context
   * @param boolean - if true ? demo : live
   * @returns {Promise<any>}
   */
  async changeGameType(context, isDemo) {
    let accountsList = [];
    context.commit('SET_GAME_TYPE', isDemo);
    Scatter.makeScatterNetwork(context.state.network);
    context.commit('grid/CLEAN_GRID', null, {root: true});
    context.commit('game/CLEAN_BETS', null, {root: true});
    req.defaults.baseURL = config.API(context.state.network);
    let accountToReturn = '';
    if (Scatter.instance && Scatter.instance.identity) {
      Eos.setScatterApi()
    } else {
      if (context.rootState.wallet.privateKey) {
        let [err, res] = await to(context.dispatch('signIn/getAccountByKey', {
          publicKey: context.rootState.wallet.publicKey,
          network: context.rootState.common.network
        }, {root: true}));
        let accountName = localStorage.getItem('accountname');
        if (!err && res.length > 0) {
          accountsList = res.filter(item => item.permission_name !== 'owner');
          accountName = accountsList[accountsList.length - 1];
          if (accountName.account_name) accountName = accountName.account_name;
          context.commit('wallet/SET_DATA', {
            name: accountName
          }, {root: true});
          localStorage.setItem('accountname', accountName);
          accountToReturn = accountName;
        } else {
          accountToReturn = 'No account';
        }
        [err, res] = await to(context.dispatch('wallet/getAccount', {
          accountName
        }, {root: true}));
        if (!err) {
          Wallet.getKeysFromPermissions(res.permissions);
        }
        Eos.setPureApi(context.rootState.wallet.privateKey)
      }
    }
    ws.changeNetwork(context.state.network);
    context.dispatch('wallet/changeCheckingSettings', {}, {root: true});
    context.dispatch('game/fetchSettings', null, {root: true});
    context.commit('SET_FIELD', {
      field: 'useFreeCpu',
      value: !isDemo
    });
    context.dispatch('game/fetchGames', {
      limit: 20,
      offset: 0,
    }, {root: true});
    context.dispatch('game/fetchLastGame', {
      limit: 1,
      offset: 0
    }, {root: true});
    Scatter.checked = true;
    context.dispatch('user/addUserInfo', {}, {root: true});
    return {
      currentAccount: accountToReturn,
      accountsList: accountsList
    };
  },
  setAccount(context, accountName = '') {
    if (!accountName) return;

    context.commit('wallet/SET_DATA', {
      name: accountName
    }, {root: true});
    context.dispatch('wallet/getAccount', {
      accountName
    }, {root: true});
    localStorage.setItem('accountname', accountName);

    context.dispatch('game/fetchGames', {
      limit: 20,
      offset: 0,
    }, {root: true});
    context.dispatch('game/fetchLastGame', {
      limit: 1,
      offset: 0
    }, {root: true});
  },
  getIpInfo() {
    return to(req({
      url: `/geo`,
      method: 'GET'
    }));
  }
};
