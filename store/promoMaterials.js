const data = [{
  size: '160x160',
  urls: ['./banners/1/234х60.png', './banners/1/234х60.png', './banners/1/234х60.png'],
  shareList: [{
    type: 'f',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'
  }, {
    type: 'i',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'
  }]
}, {
  size: '320x160',
  urls: ['./banners/1/160х600.png', './banners/1/160х600.png', './banners/1/160х600.png'],
  shareList: [{
    type: 'f',
    text: '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'
  }, {
    type: 'i',
    text: '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'
  }]
}];


export const state = () => ({
  sizes: {
    width: [160, 320],
    height: [160, 320]
  },
  data: {
    facebook: []
  },
  socialList: [{
    name: 'facebook',
    ga: 'fb',
    icon: './icons-s/f-icon.png'
  }, {
    name: 'instagram',
    ga: 'ig',
    icon: './icons-s/i-icon.png'
  }, {
    name: 'pinterest',
    ga: 'pi',
    icon: './icons-s/p-icon.png'
  }, {
    name: 'vk',
    ga: 'vk',
    icon: './icons-s/v-icon.png'
  }, {
    name: 'twitter',
    ga: 'tw',
    icon: './icons-s/t-icon.png'
  }, {
    name: 'weechat',
    ga: 'wc',
    icon: './icons-s/we-icon.png'
  }]
});
export const mutations = {
  SET_DATA(store, {field, value}) {
    store.data[field] = value;
  }
};
export const actions = {
  fetch(context, {type = 'facebook'} = {}) {
    context.commit('SET_DATA', {field: type, value: data});
  }
};
