import {
  createUser,
  createUser_new,
  sign,
  addUserInfo,
  getRefCookies,
  fetchReferralInfo,
  fetchReferralLink
} from "~/components/api/user";
import {Eos} from "~/services/provider/eos";
import to from 'await-to-js';
import Scatter from '~/services/wallet/scatter';
import Cookies from 'js-cookie';

export const state = () => ({
  data: [],
  isAuth: false,
  needSignTransaction: true
});
export const mutations = {
  SET_DATA(store, array) {
    store.data = array;
  },
  SET_LOGIN(store, boolean) {
    store.isAuth = boolean;
  },
  SET_FLAG_TRANSACTION_SIGN(store, boolean) {
    store.needSignTransaction = boolean;
  }
};
export const actions = {
  create(context, data) {
    Scatter.checked = false;
    return new Promise((resolve, reject) => {
      if (false && data.net === 'mainnet') {
        createUser_new(data).then(res => {
          resolve(res);
        }).catch(e => {
          reject(e);
        }).finally(_ => {
          Scatter.checked = true;
        });
      } else {
        createUser(data).then(res => {
          resolve(res);
        }).catch(e => {
          reject(e);
        }).finally(_ => {
          Scatter.checked = true;
        })
      }
    })
  },
  fetchReferralLink(context, url) {
    const accountName = context.rootState.wallet.accountName;
    if (accountName) {
      return fetchReferralLink({
        account: accountName,
        target: url,
        type: 'internal'
      });
    }
    return {}
  },
  async addUserInfo(context) {
    const accountName = context.rootState.wallet.accountName;
    if (accountName) {
      let [err, res] = await to(fetchReferralInfo());
      [err, res] = await to(addUserInfo({
        username: accountName,
        referral_of: res.referral_of || '',
        global_referral_of: res.global_referral_of || '',
        stat_id: res.stat_id || ''
      }));
      return true;
    }
  },
  login(context) {
    context.commit('SET_LOGIN', true);
    //Eos.setPureApi(context.rootState.wallet.privateKey);
  },
  logout(context) {
    context.commit('SET_LOGIN', false);
    context.dispatch('wallet/logout', null, {root: true});
  },
  async stackRam(context, {accountName = localStorage.getItem('accountname'), amount = 1024 * 3} = {}) {
    const actions = Eos.makeRamActions({
      accountName,
      amount
    });
    if (context.state.needSignTransaction) {
      const [err, tx] = await to(sign({
        tx: actions,
        username: accountName
      }));
      if (err) throw err;

      const [errSig, res] = await to(Eos.createSignatures({
        tx: actions,
        serverTransaction: tx
      }));
      if (errSig) throw errSig;
      return true;
    } else {
      const [err, res] = await to(Eos.api.transact(actions, {
        blocksBehind: 3,
        expireSeconds: 30,
      }));
      if (err) throw err;
      return true;
    }
  },
  async powerUp(context, {accountName = localStorage.getItem('accountname'), cpu_frac = 0, net_frac = 0, max_payment = 0} = {}) {
    const actions = Eos.makePowerupAction({
      accountName,
      cpu_frac,
      net_frac,
      max_payment
    });
    if (context.state.needSignTransaction) {
      const [err, tx] = await to(sign({
        tx: actions,
        username: accountName
      }));
      if (err) throw err;
      const [errSig, res] = await to(Eos.createSignatures({
        tx: actions,
        serverTransaction: tx
      }));
      if (errSig) throw err;
      return true;
    } else {
      const [err, res] = await to(Eos.api.transact(actions, {
        blocksBehind: 3,
        expireSeconds: 30,
      }));
      if (err) throw err;
      return true;
    }
  },

  /**
   * @deprecated
   * @param {*} context
   * @param {*} param1
   * @returns
   */
  async stackCpu(context, {accountName = localStorage.getItem('accountname'), cpu_frac = 0, net_frac = 0, max_payment = 0} = {}) {
    const actions = Eos.makeCpuActions({
      accountName,
      cpu_frac,
      net_frac,
      max_payment
    });
    if (context.state.needSignTransaction) {
      const [err, tx] = await to(sign({
        tx: actions,
        username: accountName
      }));
      if (err) throw err;
      const [errSig, res] = await to(Eos.createSignatures({
        tx: actions,
        serverTransaction: tx
      }));
      if (errSig) throw err;
      return true;
    } else {
      const [err, res] = await to(Eos.api.transact(actions, {
        blocksBehind: 3,
        expireSeconds: 30,
      }));
      if (err) throw err;
      return true;
    }
  },
  /**
   * @deprecated
   * @param {*} context
   * @param {*} param1
   * @returns
   */
  async stackNet(context, {accountName = localStorage.getItem('accountname'), amount = 0} = {}) {
    const actions = Eos.makeNetActions({
      accountName,
      amount
    });
    if (context.state.needSignTransaction) {
      const [err, tx] = await to(sign({
        tx: actions,
        username: accountName
      }));
      if (err) throw err;
      const [errSig, res] = await to(Eos.createSignatures({
        tx: actions,
        serverTransaction: tx
      }));
      if (errSig) throw errSig;
      return true;
    } else {
      const [err, res] = await to(Eos.api.transact(actions, {
        blocksBehind: 3,
        expireSeconds: 30,
      }));
      if (err) throw err;
      return true;
    }
  },
  async stackAndPowerup(context, {accountName = localStorage.getItem('accountname'), ram = 0, cpu_frac = 0, net_frac = 0, max_payment = 0} = {}) {
    const actions = [];
    if (ram) actions.push({
      account: 'eosio',
      name: 'buyrambytes',
      data: {
        payer: accountName,
        receiver: accountName,
        bytes: ram,
      },
      authorization: [{
        actor: accountName,
        permission: 'active',
      }]
    });

    const poweupActions = Eos.makePowerupAction({
      accountName,
      cpu_frac,
      net_frac,
      max_payment
    });

    if(poweupActions.actions.length) {
      actions.push(...poweupActions.actions);
    }

    if (!actions.length) return [new Error('No actions provided'), null];
    const [err, res] = await to(Eos.api.transact({
      actions: actions
    }, {
      blocksBehind: 3,
      expireSeconds: 30,
    }));
    return [err, res];
  }
};
