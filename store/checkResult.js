export const state = () => ({
  data: [],
  show: false
});
export const mutations = {
  SET_SHOWING(store, boolean) {
    store.show = boolean;
  }
};
export const actions = {
  fetch(context) {
    context.commit('SET_SHOWING', true);
  }
};
