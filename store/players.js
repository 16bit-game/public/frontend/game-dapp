import {
  fetch,
  fetchCount,
  fetchReferrals,
  fetchAllReferrals,
  fetchPlayerRating,
  fetchPlayerGlobalReferral
} from '~/components/api/players';
import to from 'await-to-js';

const map = {
  SET_REFERRALS: 'referrals',
  SET_BY_MONTH: 'byMonth',
  SET_BY_DAYS: 'byDays'
};

export const state = () => ({
  data: [],
  byMonth: [],
  byDays: [],
  count: 0,
  referrals: [],
  allReferrals: [],
  rating: 0,
  bonus: 0
});
export const mutations = {
  SET_RATING_POSITION(store, number) {
    store.rating = number;
  },
  SET_RATING_BONUS(store, number) {
    store.bonus = number;
  },
  SET_DATA(store, array) {
    store.data = array;
  },
  PUSH_DATA(store, obj) {
    store.data.push(obj);
  },
  SET_REFERRALS(store, array) {
    store.referrals = array;
  },
  SET_ALL_REFERRALS(store, array) {
    store.allReferrals = array;
  },
  SET_FIELD(store, {field, value}) {
    store[field] = value;
  },
  SET_BY_MONTH(store, array) {
    store.byMonth = array;
  },
  SET_BY_DAYS(store, array) {
    store.byDays = array;
  }
};
export const actions = {
  fetch(context, {today, limit = 10, offset = 0, order = 'desc', flush} = {}) {
    return new Promise((resolve, reject) => {
      fetch({today, limit, offset, order}).then(res => {
        /*        if (flush) context.commit('SET_DATA', []);
				res.forEach((item, index) => setTimeout(_=>{
				  context.commit('PUSH_DATA', item)
				}, index * 50));*/

        flush ? context.commit('SET_DATA', res) : context.commit('SET_DATA', [...context.state.data, ...res]);
        resolve(res);
      }).catch(e => {
        console.log('fetch error - ', e);
        reject(e);
      });
    })
  },
  fetchCount(context) {
    fetchCount(localStorage.getItem('accountname')).then(res => {
      context.commit('SET_FIELD', {
        field: 'count',
        value: res.count
      })
    }).catch(e => {
      console.log('fetchCount error - ', e);
    })
  },
  fetchReferrals(context, {month, year, commit = 'SET_REFERRALS', order = 'desc', field = 'referral_timestamp'} = {}) {
    return new Promise((resolve, reject) => {
      fetchReferrals({
        username: localStorage.getItem('accountname'),
        month,
        year,
        order,
        field
      }).then(res => {
        context.commit(commit, res);
        /*context.commit(commit, [context.state[map[commit]], ...res]);*/
        resolve(res);
      }).catch(e => {
        console.log('fetch referrals error - ', e);
        reject(e);
      })
    })
  },
  fetchAllReferrals(context, {order = 'desc', field = 'timestamp', limit = 20, offset = 0} = {}) {
    const username = localStorage.getItem('accountname') || '';
    return new Promise((resolve, reject) => {
      fetchAllReferrals({
        order,
        field,
        limit,
        offset,
        username
      }).then(res => {
        if (offset === 0) {
          context.commit('SET_ALL_REFERRALS', res);
        } else {
          context.commit('SET_ALL_REFERRALS', [...context.state.allReferrals, ...res]);
        }
        resolve(res);
      }).catch(e => {
        console.log('fetchAllReferrals - ', e);
        reject();
      })
    });
  },
  async fetchPlayerRating(context, {today = false} = {}) {
    const [err, res] = await to(fetchPlayerRating({
      username: localStorage.getItem('accountname'),
      today: today ? 'today' : ''
    }));
    if (!err) {
      context.commit('SET_RATING_POSITION', res.rating);
      context.commit('SET_RATING_BONUS', res.bonus);
    }
    return res;
  },
  async fetchPlayerGlobalReferral() {
    return await to(fetchPlayerGlobalReferral(localStorage.getItem('accountname')));
  }
};
