import axios from 'axios';
import {Wallet} from '~/services/wallet';
import Scatter from '~/services/wallet/scatter';
import config from '~/config/';
import to from 'await-to-js';

export const state = () => ({
  passed: {
    step1: false,
    step2: false,
    step3: false
  },
  mnemonic: [],
  accountname: ''
});
export const mutations = {
  SET_ACCOUNT_NAME(store, string) {
    store.accountname = string;
  },
  SET_STEP(store, {key, value}) {
    store.passed[key] = value;
  },
  SET_MNEMONIC(store, array) {
    store.mnemonic = array;
  },
  CLEAR(store) {
    store.mnemonic = [];
    store.accountname = '';
  }
};
export const actions = {
  checkUserName(context, {name, network = 'mainnet', needToCommit = true} = {}) {
    return new Promise((resolve, reject) => {
      axios.post(`https://${config.EOS.NODE[network]}/v1/chain/get_account`, {
        account_name: name
      }).then(res => {
        needToCommit && context.commit('SET_ACCOUNT_NAME', name);
        resolve(res);
      }).catch(e => {
        reject(e);
      });
    })
  },

  async getAccountsByKey(context, publicKey) {
    let [err, res] = [];
    [err, res] = await to(context.dispatch(`_getAccountByKey_mainnet`, publicKey));
    if (!err && res.length) {
      return {
        type: 'mainnet',
        data: res
      }
    }
    [err, res] = await to(context.dispatch(`_getAccountByKey_testnet`, publicKey));
    if (!err) {
      return {
        type: 'testnet',
        data: res
      }
    } else {
      return {
        type: 'testnet',
        data: []
      }
    }
  },

  getAccountByKey(context, {
    publicKey,
    network = 'testnet'
  } = {}) {
    return context.dispatch(`_getAccountByKey_${network}`, publicKey);
  },

  _getAccountByKey_testnet(context, publicKey) {
    return new Promise((resolve, reject) => {
      axios.post(`https://${Scatter.network['testnet'].host}/v1/history/get_key_accounts`, {
        public_key: publicKey
      }).then(res => {
        resolve(res.data.account_names);
      }).catch(er => {
        reject(er);
      });
    });
  },

  _getAccountByKey_mainnet(context, publicKey) {
    return new Promise((resolve, reject) => {
      axios.get(`${config.API('mainnet')}/accounts/${publicKey}`, {
        public_key: publicKey
      }).then(res => {
        resolve(res.data.payload.accounts);
      }).catch(er => {
        reject(er);
      });
    });
  },
  generateMnemonic(context) {
    const mnemonic = Wallet.generateMnemonic();
    context.commit('SET_MNEMONIC', mnemonic.split(' '));
  }
};
