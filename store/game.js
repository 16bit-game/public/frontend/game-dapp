import {
  create,
  createSignedBet,
  payCpuBet
} from "~/components/api/bet";
import {
  fetchAll,
  fetchOne,
  fetchOneByAll,
  fetchLast,
  fetchSettings,
  fetchOneGame
} from "~/components/api/games";
import {Eos} from '~/services/provider/eos';
import config from '~/config/';
import to from 'await-to-js';
import {
  sha256
} from 'eosjs-ecc';
import notification
  from '~/components/UI/Notification/main.js';
import {
  getFingerPrint,
  flooring,
  mergeByDays
} from '@/components/utils';
import colorMixer from '~/components/playTable/colorMixer';
import {waitingCellStorage} from '~/components/utils/waitingCellStorage';
import Scatter from "../services/wallet/scatter";

const grid = {
  'X': [1, 2, 3, 4, 5],
  'Y': [6, 7, 8, 9, 10],
  'Z': [11, 12, 13, 14, 15],
  'A': [1, 6, 11],
  'B': [2, 7, 12],
  'C': [3, 8, 13],
  'D': [4, 9, 14],
  'E': [5, 10, 15]
};
const canUnshiftData = ({my, wins}, stats) => {
  let able = false;
  if (my && stats.player.bets_amount) able = true;
  if (wins && (stats.player.wins.count || stats.total.wins.count)) able = true;
  if (my && wins && stats.player.wins.count) able = true;
  if (!my && !wins) able = true;
  if (!my && wins && stats.total.bets_amount) {
    delete stats.player;
    able = true;
  }
  return able;
};

export const state = () => ({
  data: [],
  fullData: [],
  byMonth: [],
  byDays: [],
  inDays: [],
  settings: {
    ttl: 60,
    cap: 240, //Dont forget color mixer
    step: 0.1,
    cell_limit: 5
  },
  betAmount: parseFloat(localStorage.getItem('defaultBet')) || 1,
  hash: '',
  currentGame: {},
  listGames: [],
  bets: {
    myTotal: 0,
    total: 0,
    myCount: 0,
    allCount: 0,
    my: [],
    all: []
  },
  filters: {
    wins: false,
    my: false
  },
  started: true,
  myResult: '',
  winNumber: 0,
  myWin: 0,
  globalGameSettings: {
    game_enabled: true
  }
});
export const mutations = {
  SET_GLOBAL_GAME_SETTINGS(store, obj) {
    store.globalGameSettings = obj;
  },
  SET_FILTER(store, {type, value}) {
    store.filters[type] = value;
  },
  SET_CURRENT_SETTINGS(store, obj) {
    store.settings = {...obj, cell_limit: 5};
    waitingCellStorage.settings = store.settings;
    colorMixer.maxBet = obj.cell_limit;
  },
  SET_DATA(store, {
    field,
    value
  }) {
    store[field] = value;
  },
  PUSH_DATA(store, {
    field,
    value
  }) {
    store[field].push(value);
  },
  SET_FULL_DATA(store, array) {
    store.fullData = array;
  },
  SET_UNSHIFT_DATA(store, {house_seed_hash, stats, win}) {
    const data = {
      house_seed_hash,
      stats,
      win,
      timestamp: new Date().getTime()
    };
    store.data.unshift(data);
    if (store.data.length > 10) store.data.pop();
    store.hash = '';
  },
  SET_BET_AMOUNT(store, number) {
    store.betAmount = number;
  },
  SET_WIN_AMOUNT(store, number) {
    store.myWin = number;
  },
  SET_CURRENT_GAME(store, obj) {
    store.started = true;
    store.currentGame = obj;
  },
  CHANGE_CURRENT_GAME(store, hash) {
    store.listGames = store.listGames.filter(i => i.game.house_seed_hash.toLowerCase() !== hash.toLowerCase());
    if (store.listGames.length) {
      let current = Object.assign({}, store.listGames[store.listGames.length - 1]);
      store.currentGame = current.game;
      store.settings = {...current.rules, cell_limit: 5};
      waitingCellStorage.settings = store.settings;
      colorMixer.maxBet = current.rules.cap / 24;
    } else {
      store.currentGame = {};
    }
  },
  SET_GAME_STATE(store, boolean) {
    store.started = boolean;
  },
  SET_GAME_STATE_RESULT(store, {type, number}) {
    store.myResult = type;
    store.winNumber = number
  },
  /**
   * @deprecated - Replace to current game
   */
  _SET_GAME_HASH(store, string) {
    store.started = true;
    store.hash = string;
  },
  ADD_STARTED_GAME(store, {rules, ...obj}) {
    store.listGames.push({
      game: obj,
      rules: rules
    });
  },
  SET_BET(store, {my, ...obj} = {}) {
    if (my) {
      store.bets.myTotal += obj.amount;
      store.bets.total += obj.amount;
      store.bets.myCount += 1;
      store.bets.allCount += 1;
      store.bets.my.push(obj);
    } else if (!my) {
      store.bets.total += obj.amount;
      store.bets.allCount += 1;
      store.bets.all.push(obj);
    }
  },
  CLEAN_BETS(store) {
    store.bets = {
      myTotal: 0,
      total: 0,
      myCount: 0,
      allCount: 0,
      my: [],
      all: []
    };
    //store.currentGame = {};
  }
};

let notif = null;

export const actions = {
  async createBet(context, {amount = 0, number = [], multiplier, my = true}) {
    if (!number.length) throw('Number is not provide');
    if (!context.state.currentGame.house_seed_hash) throw('Game has been finished');
    const username = localStorage.getItem('accountname');
    const house_seed_hash = context.state.currentGame.house_seed_hash;
    const timestamp = new Date().getTime();
    const bet = {
      username,
      number,
      amount,
      timestamp,
      signature: '',
      multiplier,
      my,
      house_seed_hash
    };
    let numberToCreate = Object.assign([], number);
    if (typeof bet.number[0] === 'string') {
      numberToCreate = context.rootState.grid.data[bet.number[0]].connected;
    }
    let useFreeCpu = context.rootState.common.useFreeCpu;
    const isDemo = context.rootState.common.demo;
    if (!isDemo && Scatter.connected) {
      useFreeCpu = false;
    }
    const currentSeed = await getFingerPrint();
    const seed = sha256(currentSeed);
    const txActions = (!isDemo && useFreeCpu) ? await Eos.makeActions({
      contract: config.EOS.CONTRACT,
      accountName: username,
      numbers: numberToCreate,
      amount,
      game: house_seed_hash,
      seed
    }) : false;
    const maxCellBet = waitingCellStorage.add(bet);
    context.dispatch('grid/setWaiting', number, {root: true});
    if (isDemo) {
      const actionData = {
        player: username,
        bet: {
          game: house_seed_hash.toLowerCase(),
          seed: seed,
          quantity: amount,
          numbers: numberToCreate
        }
      };
      let [err, res] = await to(createSignedBet(actionData));
      if (err) {
        context.dispatch('grid/removeWaiting', number, {root: true});
        waitingCellStorage.remove(bet);
        console.log('apis-error', err);
        const eMsg = (err && err.length) ? err[0].message : (err.message || 'Something went wrong');
        throw new Error(eMsg);
      }
      [err, res] = await to(Eos.createDemoBet(res));
      if (err) {
        context.dispatch('grid/removeWaiting', number, {root: true});
        waitingCellStorage.remove(bet);
        if (err.message.indexOf('sender_id') > -1) {
          context.dispatch('createBet', {
            amount,
            number,
            multiplier,
            my
          });
        } else {
          notification.warning({
            message: err,
          });
        }
        return;
      }

      context.commit('SET_BET', bet);
      context.dispatch('grid/set', {
        amount,
        number,
        my,
        maxBet: maxCellBet
      }, {root: true});
      context.commit('wallet/DECREMENT_BALANCE', amount, {root: true});
      context.dispatch('grid/removeWaiting', number, {root: true});
      return res;
    } else {
      let actionData = {
        player: username,
        bet: {
          game: house_seed_hash.toLowerCase(),
          seed: seed,
          quantity: amount,
          numbers: numberToCreate
        }
      };
      if (txActions) actionData.tx = txActions;
      let [err, res] = await to(createSignedBet(actionData));

      if (err) {
        context.dispatch('grid/removeWaiting', number, {root: true});
        waitingCellStorage.remove(bet);
        console.log('apis-error', err);
        return;
      }

      let {tx, ...signatures} = res;

      [err, res] = await to(Eos.createBet({
        isDemo,
        tx: tx,
        serverTransaction: signatures,
        accountName: username,
        numbers: numberToCreate,
        amount,
        game: house_seed_hash,
        additionalResponse: res
      }));
      console.log('ERROR - ', err);
      if (err) {
        context.dispatch('grid/removeWaiting', number, {root: true});
        waitingCellStorage.remove(bet);
        if (err.message.indexOf('sender_id') > -1) {
          context.dispatch('createBet', {
            amount,
            number,
            multiplier,
            my
          });
        } else {
          notification.warning({
            message: err,
          });
        }
        return;
      }

      context.commit('SET_BET', bet);
      context.dispatch('grid/set', {
        amount,
        number,
        my
      }, {root: true});
      context.commit('wallet/DECREMENT_BALANCE', amount, {root: true});
      context.dispatch('grid/removeWaiting', number, {root: true});
      return res;
    }
  },
  restoreBet(context, {amount = 0, number = [], my = true, username = ''}) {
    return new Promise((resolve, reject) => {
      if (!number.length) reject('number is not provide');
      const timestamp = new Date().getTime();

      let multiplier = 15;
      if (number.length > 1) {
        Object.keys(context.rootState.grid.data).forEach(item => {
          if (JSON.stringify(context.rootState.grid.data[item].connected) === JSON.stringify(number)) {
            number = [item];
            multiplier = 15 / context.rootState.grid.data[item].connected.length;
          }
        });
      }

      const bet = {
        username,
        number,
        amount: amount,
        timestamp,
        signature: '',
        multiplier,
        my
      };
      waitingCellStorage.add(bet);
      context.commit('SET_BET', bet);
      context.dispatch('grid/set', {
        amount,
        number,
        my,
        linked: true,
        maxBet: my ? context.state.settings.cell_limit : context.state.settings.cap / 24
      }, {root: true});
      resolve();
    });
  },
  alienBet(context, {amount = 0, number = [], my = false, username = ''}) {
    return new Promise((resolve, reject) => {
      if (!number.length) reject('number is not provide');
      const timestamp = new Date().getTime();

      let multiplier = 15;
      if (number.length > 1) {
        Object.keys(context.rootState.grid.data).forEach(item => {
          if (JSON.stringify(context.rootState.grid.data[item].connected) === JSON.stringify(number)) {
            number = [item];
            multiplier = 15 / context.rootState.grid.data[item].connected.length;
          }
        });
      }

      const bet = {
        username,
        number,
        amount: amount,
        timestamp,
        signature: '',
        multiplier,
        my
      };
      context.commit('SET_BET', bet);
      context.dispatch('grid/set', {
        amount,
        number,
        my,
        linked: false
      }, {root: true});
      resolve();
    });
  },
  fetchFullGames(context, {limit = 21, offset = 0, flush = false, animate = false, commit = 'SET_DATA', field = 'data', byUser = false, wins = undefined, day, month, year} = {}) {
    byUser = context.state.filters.my;
    //wins = !context.state.filters.wins ? undefined : context.state.filters.wins;
    wins = undefined;
    let username = localStorage.getItem('accountname') || '';
    if (context.state.filters.wins) {
      wins = context.state.filters.wins;
      //!byUser ? username = null : null;
    }
    return new Promise((resolve, reject) => {
      fetchAll({
        limit,
        offset,
        order: 'desc',
        byUser,
        month,
        year,
        wins,
        day,
        username
      }).then(res => {
        //(!byUser && offset === 0) ? res.shift() : null;
        //if (!res.length) return resolve(res);
        if (flush) context.commit(commit, {
          field,
          value: []
        });
        let data = [];
        if (field === 'byDays') {
          data = mergeByDays({
            data: res,
            year: new Date().getFullYear(),
            month: month.split('/')[1]
          });
        } else {
          data = res;
        }

        if (animate) {
          data.forEach((item, index) => setTimeout(_ => {
            context.commit('PUSH_DATA', {
              field,
              value: item
            })
          }, index * 50));
        } else {
          context.commit(commit, {
            field,
            value: [...context.state[field], ...data]
          });
        }
        resolve(data);
      }).catch(e => {
        console.log('fetch games error', e);
        reject(e);
      });
    });
  },
  fetchGames(context, {limit = 20, offset = 0, commit = 'SET_DATA', field = 'data', byUser = false, wins = undefined, restoreData = true, month, year, day} = {}) {
    byUser = context.state.filters.my;
    //wins = !context.state.filters.wins ? undefined : context.state.filters.wins;
    wins = undefined;
    let username = localStorage.getItem('accountname') || '';
    if (context.state.filters.wins) {
      wins = context.state.filters.wins;
      //!byUser ? username = null : null;
    }
    return new Promise((resolve, reject) => {
      fetchAll({
        limit,
        offset,
        order: 'desc',
        byUser,
        month,
        year,
        wins,
        day,
        username: username
      }).then(res => {
        if (!res.length) {
          context.commit(commit, {field, value: res});
          return resolve(res);
        }
        context.commit(commit, {field, value: res});
        resolve(res);
      }).catch(e => {
        console.log('fetch games error', e);
        reject(e);
      });
    });
  },
  fetchLastGame(context) {
    context.dispatch('grid/setShadowWin', {number: ''}, {root: true});
    return new Promise((resolve, reject) => {
      fetchLast({
        order: 'desc'
      }).then(res => {
        const firstElement = res.shift();
        if (firstElement) {
          context.commit('SET_CURRENT_GAME', firstElement);
          context.commit('SET_CURRENT_SETTINGS', firstElement.rules);
          context.dispatch('fetchGame', {
            hash: firstElement.house_seed_hash,
            my: false
          });
          context.dispatch('fetchGame', {
            hash: firstElement.house_seed_hash,
            my: true
          });
        } else {
          context.commit('SET_CURRENT_GAME', {});
        }
        resolve(res);
      }).catch(err => {
        reject(err);
      });
    })
  },
  async fetchGameInfo(context, hash) {
    return to(fetchOneGame(hash));
  },
  async fetchGame(context, {hash, my}) {
    const username = localStorage.getItem('accountname') || '';
    const query = username ? fetchOne : fetchOneByAll;
    if (!username && my) return;
    query({
      hash,
      username,
      other: !my ? '/other' : ''
    }).then(async res => {
      for(let i = 0; i<res.length; i++) {
        const item = res[i];
        if (item.player !== localStorage.getItem('accountname')) my = false;
        await context.dispatch(my ? 'restoreBet' : 'alienBet', {
          amount: item.amount,
          number: item.number,
          multiplier: 15,
          my: my,
          username: username
        });
      }
      return res
    }).catch(e => {
      console.log('fetch games error', e);
      throw new Error(e);
    });
  },
  start(context, data) {
    if (!context.state.currentGame.house_seed_hash/* || (data.timestamp - context.state.currentGame.timestamp) / 1000 > 90*/) {
      context.commit('SET_CURRENT_GAME', {
        house_seed_hash: data.house_seed_hash,
        timestamp: data.timestamp
      });
      context.commit('SET_CURRENT_SETTINGS', data.rules);
    } else {
      context.commit('ADD_STARTED_GAME', data);
    }
  },
  finish(context, {win, hash} = {}) {
    //context.commit('SET_GAME_STATE', false);
    context.dispatch('grid/removeWaiting', [{}], {root: true});
    context.dispatch('grid/setShadowWin', {number: ''}, {root: true});

    context.dispatch('grid/finishGame', win, {root: true}).then(res => {
      const stats = {
        total: {
          bets_amount: 0,
          wins: {
            count: 0,
            amount: 0
          },
          losses: {
            count: 0,
            amount: 0
          }
        },
        player: {
          bets_amount: 0,
          wins: {
            count: 0,
            amount: 0
          },
          losses: {
            count: 0,
            amount: 0
          }
        }
      };
      context.state.bets.all.forEach(item => {
        let number = item.number;
        if (typeof number[0] === 'string') number = grid[number[0].toUpperCase()];
        if (number.indexOf(win) > -1) {
          stats.total.wins.count += 1;
          stats.total.wins.amount += item.amount * item.multiplier;
        } else {
          stats.total.losses.count += 1;
          stats.total.losses.amount += item.amount;
        }
        stats.total.bets_amount += item.amount;
      });
      if (context.state.bets.my.length) {
        context.state.bets.my.forEach(item => {
          let number = item.number;
          if (typeof number[0] === 'string') number = grid[number[0].toUpperCase()];
          if (number.indexOf(win) > -1) {
            stats.player.wins.count += 1;
            stats.player.wins.amount += item.amount * item.multiplier;
            stats.total.wins.count += 1;
            stats.total.wins.amount += item.amount * item.multiplier;
          } else {
            stats.player.losses.count += 1;
            stats.player.losses.amount += item.amount;
            stats.total.losses.count += 1;
            stats.total.losses.amount += item.amount;
          }
          stats.player.bets_amount += item.amount;
          stats.total.bets_amount += item.amount;
        });
        stats.player.wins.amount = flooring(stats.player.wins.amount);
        stats.total.wins.amount = flooring(stats.total.wins.amount);
        stats.player.bets_amount = flooring(stats.player.bets_amount);
        stats.total.bets_amount = flooring(stats.total.bets_amount);

        if (stats.player.wins.count) {
          context.commit('SET_GAME_STATE_RESULT', {
            type: 'win',
            number: win
          });
          context.commit('SET_WIN_AMOUNT', stats.player.wins.amount);
        } else if (context.state.bets.my.length) {
          context.commit('SET_GAME_STATE_RESULT', {
            type: 'lose',
            number: win
          });
          context.commit('SET_WIN_AMOUNT', 0);
        }

        if (canUnshiftData(context.state.filters, stats)) {
          context.commit('SET_UNSHIFT_DATA', {
            house_seed_hash: hash,
            stats,
            win
          });
        }

      } else {
        if (canUnshiftData(context.state.filters, stats)) {
          context.commit('SET_UNSHIFT_DATA', {
            house_seed_hash: hash,
            stats,
            win
          });
        }
      }
      context.commit('grid/CLEAN_GRID', null, {root: true});
      context.commit('CLEAN_BETS');
      context.commit('CHANGE_CURRENT_GAME', hash);
      context.dispatch('fetchSettings', null);
      waitingCellStorage.clear();
      context.rootState.wallet.accountName && context.dispatch('wallet/checkBalance', {
        accountname: context.rootState.wallet.accountName
      }, {root: true});
    });
  },
  async fetchSettings({commit, rootState}) {
    const network = rootState.common.network;
    const [err, res] = await to(fetchSettings(network));
    if (!err && res) {
      commit('SET_GLOBAL_GAME_SETTINGS', res);
    }
    return [err, res];
  }
};

export const getters = {
  myBets(state, getters, rootState) {
    const data = state.bets.my;
    const output = {};
    const limiter = state.settings.cell_limit;
    data.forEach(item => {
      if (!output[item.number[0]]) {
        output[item.number[0]] = {
          number: item.number[0],
          amount: item.amount,
          progress: (parseInt(item.amount / (limiter) * 10000) / 100) + '%',
          win: parseInt(item.amount * item.multiplier * 100) / 100
        }
      } else {
        output[item.number[0]].progress = (parseInt(output[item.number[0]].progress) + parseInt(item.amount / (limiter) * 10000) / 100) + '%';
        output[item.number[0]].amount += item.amount;
        output[item.number[0]].win += parseInt(item.amount * item.multiplier * 100) / 100;
      }
    });
    return Object.values(output);
  }
};
