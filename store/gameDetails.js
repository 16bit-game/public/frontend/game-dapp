import {
  fetchOne,
  fetchOneByAll
} from '~/components/api/games';

export const state = () => ({
  details: {
    win: '',
    house_seed_hash: '',
    house_seed: '',
    compound_hash: '',
    timestamp: '',
    stats: {},
    my: [],
    others: []
  },
  show: false,
});
export const mutations = {
  SET_SHOWING(store, boolean) {
    store.show = boolean;
  },
  SET_HISTORY_GAME_SETTINGS(store, {house_seed_hash, house_seed, compound_hash, win, stats, timestamp}) {
    store.details.house_seed_hash = house_seed_hash;
    store.details.house_seed = house_seed;
    store.details.compound_hash = compound_hash;
    store.details.win = win;
    store.details.stats = stats;
    store.details.timestamp = timestamp;
  },
  SET_GAME_DETAILS_BETS(store, {field = 'my', data = []} = {}) {
    store.details[field] = data;
  },
  CLEAN_DETAILS(store) {
    store.details = {
      win: '',
      house_seed_hash: '',
      house_seed: '',
      compound_hash: '',
      stats: {},
      my: [],
      others: []
    }
  }
};
export const actions = {
  fetch(context, {hash, type}) {
    return new Promise((resolve, reject) => {

      const username = localStorage.getItem('accountname') || '';
      const query = username ? fetchOne : fetchOneByAll;
      query({
        hash,
        username,
        other: (type === 'others') ? '/other' : ''
      }).then(res => {
        //const wins = res.bets.filter(item => item.win);
        /*if (res.bets.all && username) {
          res.bets.all = res.bets.all.filter(item => item.username !== username);
        }*/
        context.commit('SET_GAME_DETAILS_BETS', {
          field: type,
          data: res
        });
        resolve();
      }).catch(e => {
        console.log('fetch one game error', e);
        reject();
      })
    })
  }
};

const getStringCell = (state, numbers) => {
  let number = [];
  let multiplier = 15;
  Object.keys(state.grid.data).forEach(item => {
    if (JSON.stringify(state.grid.data[item].connected) === JSON.stringify(numbers)) {
      number = [item];
      multiplier = 15 / state.grid.data[item].connected.length;
    }
  });
  return {number, multiplier};
};


export const getters = {
  details(state, getters, rootState) {
    const output = {
      ...state.details,
      myWins: [],
      myLosses: [],
      otherWins: [],
      otherLosses: [],
    };

    const myWins = {};
    const myLosses = {};
    const otherWins = {};
    const otherLosses = {};

    state.details.my.forEach(item => {
      let number = item.number;
      let multiplier = 15;
      if (item.number.length > 2) {
        const r = getStringCell(rootState, item.number);
        number = r.number;
        multiplier = r.multiplier;
      }
      number = number[0];
      item.showingNumber = number;
      item.multiplier = multiplier;
      if (item.win) {
        if (!myWins[number]) {
          myWins[number] = {
            ...item,
            sum: parseInt(item.amount * item.multiplier * 1000) / 1000
          }
        } else {
          myWins[number].amount += item.amount;
          myWins[number].sum += parseInt(item.amount * item.multiplier * 1000) / 1000;
        }
      } else {
        if (!myLosses[number]) {
          myLosses[number] = {
            ...item,
            sum: parseInt(item.amount * item.multiplier * 1000) / 1000
          }
        } else {
          myLosses[number].amount += item.amount;
          myLosses[number].sum += parseInt(item.amount * item.multiplier * 1000) / 1000;
        }
      }
    });
    state.details.others.forEach(item => {
      let number = item.number;
      let multiplier = 15;
      if (item.number.length > 2) {
        const r = getStringCell(rootState, item.number);
        number = r.number;
        multiplier = r.multiplier;
      }
      number = number[0];
      item.showingNumber = number;
      item.multiplier = multiplier;
      if (item.win) {
        if (!otherWins[number]) {
          otherWins[number] = {
            ...item,
            sum: parseInt(item.amount * item.multiplier * 1000) / 1000
          }
        } else {
          otherWins[number].amount += item.amount;
          otherWins[number].sum += parseInt(item.amount * item.multiplier * 1000) / 1000;
        }
      } else {
        if (!otherLosses[number]) {
          otherLosses[number] = {
            ...item,
            sum: parseInt(item.amount * item.multiplier * 1000) / 1000
          }
        } else {
          otherLosses[number].amount += item.amount;
          otherLosses[number].sum += parseInt(item.amount * item.multiplier * 1000) / 1000;
        }
      }
    });

    output.myWins = Object.values(myWins);
    output.myLosses = Object.values(myLosses);
    output.otherWins = Object.values(otherWins);
    output.otherLosses = Object.values(otherLosses);
    return output;
  }
};
