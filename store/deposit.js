import {
  rates,
  deposit,
  estimated,
  minRate,
  processingSwitchere
} from '~/components/api/deposit';
import to from 'await-to-js';

export const state = () => ({
  data: []
});
export const mutations = {
  SET_DATA(store, array) {
    store.data = array;
  }
};
export const actions = {
  async fetch(context) {
    try {
      const res = await rates();
      const data = res.data/*.filter(i => i.isAvailable)*/;
      data.length = 108;
      context.commit('SET_DATA', data);
    } catch (e) {
      throw new Error(e);
    }
  },
  async send(context, data) {
    const [err, res] = await to(deposit(data));
    return [err, res];
  },
  async estimated(context, data) {
    try {
      const res = await estimated(data);
      if (res.error) throw new Error(res.error);
      return res.estimatedAmount;
    } catch (e) {
      throw new Error(e);
    }
  },
  async minRate(context, from) {
    const [err,res] = await to(minRate(from));
    return [err,res];
  },
  processingSwitchere(_, data) {
    return to(processingSwitchere(data));
  }
};
