import {Wallet} from '~/services/wallet';
import {Eos} from "~/services/provider/eos";
import axios from "axios/index";
import Scatter from '~/services/wallet/scatter';
import ws from '~/services/ws';
import to from 'await-to-js';

import config from '~/config/';
import {fauset, fausetBonus} from '~/components/api/user';

import worker
  from '~/components/api/workers/checkAccountInstance';

let workerInited = false;

const formatDataObject = (data, state, zero = false) => {
  return {
    net: {
      eos: !zero ? 0 : (parseFloat(data.total_resources ? data.total_resources.net_weight : 0) || 0),
      used: !zero ? 0 : (parseFloat(data.net_limit.used) || 0),
      max: !zero ? 0 : (parseFloat(data.net_limit.max) || 0)
    },
    cpu: {
      eos: !zero ? 0 : (parseFloat(data.total_resources ? data.total_resources.cpu_weight : 0) || 0),
      used: !zero ? 0 : (parseFloat(data.cpu_limit.used) || 0),
      max: !zero ? 0 : (parseFloat(data.cpu_limit.max) || 0)
    },
    ram: {
      eos: !zero ? 0 : (parseInt(data.ram_quota * state.price.ram * 1000) / 1000 || 0),
      max: !zero ? 0 : (parseFloat(data.ram_quota) || 0),
      used: !zero ? 0 : (parseFloat(data.ram_usage) || 0)
    },
    name: !zero ? '' : data.account_name
  }
};

export const state = () => ({
  publicKey: localStorage.getItem('public') || '',
  privateKey: localStorage.getItem('private') || '',
  accountName: localStorage.getItem('accountname') || '',
  balance: 0,
  tempAccountName: localStorage.getItem('accountname') | '',
  price: {
    ram: 0.00001
  },
  resources: {
    net: {
      eos: 0,
      used: 0,
      max: 0
    },
    ram: {
      eos: 0,
      used: 0,
      max: 0
    },
    cpu: {
      eos: 0,
      used: 0,
      max: 0
    }
  },
  calculatedResources: {
    net: {
      eos: 0,
      used: 0,
      max: 0
    },
    ram: {
      eos: 0,
      used: 0,
      max: 0
    },
    cpu: {
      eos: 0,
      used: 0,
      max: 0
    }
  }
});
export const mutations = {
  SET_TEMPACCOUNT_NAME(store, string) {
    store.tempAccountName = string;
  },
  SET_PUBKEY(store, string) {
    if (typeof string === 'string') {
      store.publicKey = string
    } else if (typeof string === 'object') {
      store.publicKey = string[0];
    }
  },
  SET_PRIVATE_KEY(store, string) {
    if (typeof string === 'string') {
      store.privateKey = string
    } else if (typeof string === 'object') {
      store.privateKey = string[0];
    }
  },
  /**
   * @repdecated
   */
  SET_BALANCE(store, number) {
    store.balance = number;
  },
  DECREMENT_BALANCE(store, number) {
    store.balance -= number;
  },
  SET_RAM_PRICE(store, number) {
    store.price.ram = number;
  },
  SET_CALCULATED_RESOURCES(store, {net, cpu, ram}) {
    if (net || net === 0) store.calculatedResources.net = net;
    if (cpu || cpu === 0) store.calculatedResources.cpu = cpu;
    if (ram || ram === 0) store.calculatedResources.ram = ram;
  },
  SET_DATA(store, {net, cpu, ram, balance, name} = {}) {
    if (net || net === 0) store.resources.net = net;
    if (cpu || cpu === 0) store.resources.cpu = cpu;
    if (ram || ram === 0) store.resources.ram = ram;
    if (name) {
      store.accountName = name;
    } else if (name === '') {
      store.accountName = 'No account';
    }
    if (balance || balance === 0) {
      localStorage.setItem('balance', balance);
      store.balance = balance;
    }
  }
};
export const actions = {
  async init(context) {
    const initialNetwork = context.rootState.common.demo ? 'testnet' : 'mainnet';
    Scatter.makeScatterNetwork(initialNetwork);
    const [err, connectedToScatter] = await to(Scatter.init());
    if (!err && connectedToScatter) {
      const user = Scatter.getEosAccount();
      if (user) {
        //Set LIVE game if we have scatter user (default);
        localStorage.setItem('accountname', user.name);
        localStorage.setItem('public', user.publicKey);
        context.commit('SET_PUBKEY', user.publicKey);
        context.commit('SET_DATA', {
          name: user.name
        });
        context.commit('user/SET_LOGIN', true, {root: true});
        await context.dispatch('common/changeGameType', false, {root: true});
      }
      Eos.setScatterApi();
    }


    if (!connectedToScatter) {
      if (context.state.privateKey) {
        await context.dispatch('loginByKey');
      } else {
        await context.dispatch('common/changeGameType', initialNetwork === 'testnet' ? true : false, {root: true});
        ws.changeNetwork(context.rootState.common.network || initialNetwork);
      }
    } else {
      ws.changeNetwork(context.rootState.common.network || initialNetwork);
    }
    Scatter.checked = true;
  },
  async loginByKey(context) {
    try {
      const publicKey = Wallet.privateToPublic(context.state.privateKey);
      context.commit('SET_PUBKEY', publicKey[0]);
      await context.dispatch('common/changeGameType', context.rootState.common.demo, {root: true});
      localStorage.setItem('public', publicKey[0]);
      context.commit('user/SET_LOGIN', true, {root: true});
    } catch (e) {
      await context.dispatch('common/changeGameType', context.rootState.common.demo, {root: true});
      console.log(e);
      throw e;
    }
  },


  async loginScatter(context) {
    const [err, user] = await to(Scatter.login());
    if (!err) {
      localStorage.setItem('accountname', user.name);
      localStorage.setItem('public', user.publicKey);
      context.commit('SET_PUBKEY', user.publicKey);
      context.commit('SET_DATA', {
        name: user.name
      });
      context.commit('user/SET_LOGIN', true, {root: true});
      return user;
    } else {
      context.dispatch('logout');
      throw err;
    }
  },
  async suggestNetwork(context) {
    if (!Scatter.instance) return;
    if (Scatter.instance.logout) {
      await Scatter.instance.forgetIdentity();
      await Scatter.instance.logout();
    }
    Scatter.connected = false;
    await Scatter.connect();
    Eos.setScatterApi();
    //await ScatterWallet.scatter.suggestNetwork(ScatterWallet.network.scatter);
    //const r = await ScatterWallet.scatter.linkAccount(ScatterWallet.scatter.identity.accounts[0], ScatterWallet.network.scatter);
    const res = await Scatter.login();
  },
  async getCalculatedResources({dispatch, commit, state}) {
    const data = await dispatch('getAccount', {
      accountName: 'fairbet.game',
      needCommit: false,
      needCheckExisting: false
    });
    commit('SET_CALCULATED_RESOURCES', formatDataObject(data, state, true));
  },

  async getAccount(context, {accountName = '', fullHost = Scatter.network.current.fullhost(), needCommit = true, needCheckExisting = true} = {}) {
    if (!accountName) throw 'Account name hadn\'t provided';
    /**
     * TODO: Scatter RPC and pure Api RPC dont match
     * const [err, account] = await to(Eos.api.get_account(accountName));
     */
    const [err, account] = await to(axios.post(`${fullHost}/v1/chain/get_account`, {
      account_name: accountName
    }));
    if (err) {
      context.commit('SET_DATA', {
        balance: 0
      });
      context.commit('SET_DATA', formatDataObject({}, context.state, false));
      needCheckExisting && context.commit('common/SET_ACCOUNT_EXISTING', false, {root: true});
      throw err;
    } else {
      if (needCommit) {
        context.dispatch('changeCheckingSettings', {
          accountName: accountName
        });
        context.dispatch('checkBalance', {accountname: accountName});
        context.commit('SET_DATA', formatDataObject(account.data, context.state, true));
      }
      needCheckExisting && context.commit('common/SET_ACCOUNT_EXISTING', true, {root: true});
      return account.data;
    }
  },
  async checkBalance(context, {accountname = '', needCommit = true} = {}) {
    const accountName = accountname || context.state.accountName;
    if (!accountName || accountName === 'No account') {
      return console.log('No account name provided');
    }
    const [err, res] = await to(axios.post(`${Scatter.network.current.fullhost()}/v1/chain/get_currency_balance`, {
      code: "eosio.token",
      account: accountName
    }));
    if (!err) {
      if (needCommit) {
        if (!parseFloat(res.data[0]) || parseFloat(res.data[0]) < 30) context.dispatch('fausetBalance');
        context.commit('SET_DATA', {
          balance: parseFloat(res.data[0] || 0)
        });
      }
      return parseFloat(res.data[0] || 0)
    }
    return 0
  },
  fausetBalance(context) {
    const accountName = context.state.accountName;
    if (context.rootState.common.demo && accountName) {
      fauset(accountName).then(_ => {
        context.dispatch('checkBalance');
      });
    }
  },
  async fausetBonus(context) {
    const accountName = context.state.accountName;
    if (context.rootState.common.demo && accountName) {
      const [err, res] = await to(fausetBonus(accountName));
      if (err) throw new Error(err);
      context.dispatch('checkBalance');
      return res;
    }
  },
  startCheckingAccount(context) {
    if (workerInited) {
      return context.dispatch('changeCheckingSettings');
    }
    workerInited = true;
    worker.onmessage = (msg) => {
      const res = msg.data;
      if (res.success) {
        context.commit('SET_DATA', formatDataObject(res.data, context.state, true));
        context.dispatch('checkBalance');
      } else {
        context.commit('SET_DATA', formatDataObject({}, context.state, false))
      }
    };
  },
  changeCheckingSettings(context, {url = Scatter.network.current.fullhost() + '/v1/chain/get_account', accountName = context.state.accountName} = {}) {
    worker.postMessage({
      url,
      accountName,
      msg: 'check'
    });
  },
//TODO check save keys because Wallet.publicKey now undefined
  saveKeys(context, {
    publicKey = Wallet.publicKey,
    privateKey = Wallet.privateKey
  } = {}) {
    context.commit('SET_PUBKEY', publicKey);
    context.commit('SET_PRIVATE_KEY', privateKey);
    localStorage.setItem('public', publicKey);
    localStorage.setItem('private', privateKey);
    localStorage.removeItem('encryptedPrivate');
    localStorage.removeItem('pincode');
  },
  async logout(context) {
    context.commit('SET_PUBKEY', '');
    context.commit('SET_PRIVATE_KEY', '');
    Scatter.logout();
    localStorage.removeItem('public');
    localStorage.removeItem('private');
    localStorage.removeItem('accountname');
    localStorage.removeItem('pincode');
    localStorage.removeItem('encryptedPrivate');
    localStorage.removeItem('balance');
    localStorage.removeItem('balance');
    worker.postMessage({
      msg: 'clear'
    });
    context.commit('SET_DATA', {
      name: '',
      balance: 0
    });
    Eos.clearState();
    context.commit('user/SET_LOGIN', false, {root: true});
  },
  checkRamPrice(context) {
    Eos.getRamPrice().then(res => {
      const row = res[0];
      const pricePerByte = parseFloat(row.quote.balance) / parseFloat(row.base.balance);
      context.commit('SET_RAM_PRICE', pricePerByte)
    });
  },
  transfer(context, {toAccount, amount, memo}) {
    const accountName = context.state.accountName;
    return to(Eos.transfer({
      from: accountName,
      to: toAccount,
      amount,
      memo
    }));
  },
  async getRexInfo() {
    try {
      const res = await Eos.getRexInfo();
      if (res.error) throw new Error(res.error);
      return res.data.rows[0];
    } catch (e) {
      throw new Error(e);
    }
  }
};
