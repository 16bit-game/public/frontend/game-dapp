import sCrypt from 'scrypt-js';
import buffer from 'buffer';
import aesjs from 'aes-js';

import {Eos} from "~/services/provider/eos";

export const state = () => ({
  show: localStorage.getItem('pincode') === 'true',
  controls: localStorage.getItem('pincode') !== 'true',
  navs: localStorage.getItem('pincode') !== 'true'
});
export const mutations = {
  SET_DATA(store, array) {
    store.data = array;
  },
  SET_SHOW_PINCODE(store, {show = true, controls = true, navs = true} = {}) {
    store.show = show;
    store.controls = controls;
    store.navs = navs;
  }
};
export const actions = {
  encrypt(context, {privateKey = localStorage.getItem('private'), pin = '0000'} = {}) {
    return new Promise((resolve, reject) => {
      if (!pin || !privateKey) {
        return;
      }
      const N = 1024, r = 8, p = 1;
      const dkLen = 32;
      const password = new buffer.SlowBuffer(pin.normalize('NFKC'));
      const salt = new buffer.SlowBuffer(location.hostname.normalize('NFKC'));
      const key = sCrypt.syncScrypt(password, salt, N, r, p, dkLen);

      const aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(1));
      const pkey = aesjs.utils.utf8.toBytes(privateKey);
      const enctypted = aesCtr.encrypt(pkey);
      const encryptedHex = aesjs.utils.hex.fromBytes(enctypted);
      localStorage.setItem('pincode', true);
      localStorage.setItem('encryptedPrivate', encryptedHex);
      localStorage.removeItem('private');
      resolve();
    })
  },
  decrypt(context, {hex = localStorage.getItem('encryptedPrivate'), pin = '0000'} = {}) {
    return new Promise((resolve, reject) => {
      if (!hex) reject('encrypted key not provided');
      const N = 1024, r = 8, p = 1;
      const dkLen = 32;
      const password = new buffer.SlowBuffer(pin.normalize('NFKC'));
      const salt = new buffer.SlowBuffer(location.hostname.normalize('NFKC'));
      const key = sCrypt.syncScrypt(password, salt, N, r, p, dkLen);
      const encryptedBytes = aesjs.utils.hex.toBytes(hex);
      const aesCtr2 = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(1));
      const decryptedBytes = aesCtr2.decrypt(encryptedBytes);
      const decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
      resolve(decryptedText);
    });
  },
  lock(context) {
    Eos.clearState();
    context.commit('SET_SHOW_PINCODE', {
      show: false,
      controls: false
    })
  },
  unlock(context, {pin}) {
    return new Promise((resolve, reject) => {
      context.dispatch('decrypt', {pin}).then(privateKey => {
        const matched = privateKey.match(/[a-zA-Z1-9]/gi);
        if (!matched || matched.length < 51) return reject({
          msg: 'decrypted key not valid',
          body: privateKey
        });
        context.commit('wallet/SET_PRIVATE_KEY', privateKey, {root: true});
        localStorage.setItem('private', privateKey);
        //Eos.init(privateKey);
        /*          context.dispatch('wallet/init', {
                    privateKey: privateKey
                  }, {root: true});*/
        context.commit('SET_SHOW_PINCODE', {
          show: false
        });
        resolve();
      });
    });
  }
};

