import colorMixer from '~/components/playTable/colorMixer';
import inArray from '~/components/utils/inArray';

export const state = () => ({
  data: {
    'x': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [1, 2, 3, 4, 5],
    },
    1: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [],
    },
    2: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [],
    },
    3: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [],
    },
    4: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [],
    },
    5: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [],
    },
    'y': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [6, 7, 8, 9, 10]
    },
    6: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    7: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    8: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    9: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    10: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    'z': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [11, 12, 13, 14, 15]
    },
    11: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    12: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    13: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    14: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    15: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    0: {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: []
    },
    'a': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [11, 6, 1]
    },
    'b': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [12, 7, 2]
    },
    'c': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [13, 8, 3]
    },
    'd': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [14, 9, 4]
    },
    'e': {
      amount: 0,
      background: '',
      gradientInner: '',
      backgroundInner: '',
      borderColor: '',
      borderWidth: 2,
      shadow: '',
      shadowInner: '',
      color: '',
      connected: [15, 10, 5]
    },
  },
  shadowWin: null,
  waiting: []
});
export const mutations = {
  SET_GRID_SETTINGS(store, {key, amount, my, linked = true, maxBet = colorMixer.maxBet} = {}) {
    try {
      const bets = this.state.game.bets;
      store.data[key].amount += amount;
      if (typeof key === 'string' && linked) {
        store.data[key].connected.forEach(item => {
          if (store.data[item].borderWidth !== 2) {
            store.data[item].borderWidth = 1;
          } else {
            if (!store.data[item].borderColor) store.data[item].borderColor = 'rgba(0, 186, 195, .4);';
          }
        });
      }

      if (my && inArray(bets.my, key)) {
        if (store.data[key].amount > maxBet) maxBet = store.data[key].amount;
        store.data[key].background = colorMixer.changeBackgroundColor(store.data[key].amount, maxBet);
        store.data[key].backgroundInner = colorMixer.changeBackgroundColor(store.data[key].amount, maxBet);
        store.data[key].shadow = colorMixer['shadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'shadow', maxBet) + ', ' + colorMixer['myShadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'myShadow', maxBet);
        //store.data[key].shadowInner = '';
        store.data[key].borderColor = colorMixer.myBorder.color;
        //store.data[key].borderColorInner = '';
        store.data[key].borderWidth = 2;
        store.data[key].color = colorMixer.changeColor(store.data[key].amount, 'fontColor', maxBet);
      } else {
        store.data[key].background = colorMixer.changeBackgroundColor(store.data[key].amount);
        store.data[key].backgroundInner = colorMixer.changeBackgroundColor(store.data[key].amount);
        store.data[key].shadow = colorMixer['shadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'shadow');
        store.data[key].gradientInner = colorMixer.changeGradient(store.data[key].amount, 'gradient');
        store.data[key].borderColor = !store.data[key].borderColor ? colorMixer.changeBorder(store.data[key].amount, 'border') : store.data[key].borderColor;
        store.data[key].shadowInner = colorMixer['innerShadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'innerShadow');
        //store.data[key].shadow = (colorMixer['shadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount)) : (colorMixer['shadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'shadow') + ', ' + colorMixer['myShadow'].default + ' ' + colorMixer.changeShadow(store.data[key].amount, 'myShadow'));
        //store.data[key].borderColorInner = colorMixer.changeBorder(store.data[key].amount, 'innerBorder');
        store.data[key].color = colorMixer.changeColor(store.data[key].amount, 'fontColor');
      }
    } catch (e) {
      console.log('SET_GRID error', e);
    }
  },
  CHECK_CELL_WIN(store, win) {
    store.data[win].borderColor = 'rgb(54, 210, 83)';
    store.data[win].shadow = 'rgba(0, 0, 0, 0.996) 2.5px 4.3px 20px 0px, rgb(54, 210, 83) 0px 0px 8.5px 0px';
  },
  SELECT_CELL_WIN(store, win) {
    store.data[win].background = 'rgb(54, 210, 83)';
    store.data[win].borderColor = 'rgb(54, 210, 83)';
    store.data[win].backgroundInner = 'rgb(54, 210, 83)';
    store.data[win].color = 'rgb(47, 61, 87)';
    store.data[win].shadow = '';
    store.data[win].gradientInner = '';
    store.data[win].shadowInner = '';
    if (this.state.grid.data[win].connected.length) {
      this.state.grid.data[win].connected.forEach(item => {
        !this.state.grid.data[item].amount && this.commit('grid/CLEAN_ONE_CELL', item);
      })
    }
  },
  SELECT_CELL_LOSE(store, win) {
    store.data[win].background = 'rgb(210, 54, 54)';
    store.data[win].backgroundInner = 'rgb(210, 54, 54)';
    store.data[win].borderColor = 'rgb(210, 54, 54)';
    store.data[win].color = 'rgb(255, 255, 255)';
    store.data[win].shadow = '';
    store.data[win].gradientInner = '';
    store.data[win].shadowInner = '';
    if (this.state.grid.data[win].connected.length) {
      this.state.grid.data[win].connected.forEach(item => {
        !this.state.grid.data[item].amount && this.commit('grid/CLEAN_ONE_CELL', item);
      })
    }
  },
  SELECT_CELL_OTHER(store, win) {
    store.data[win].background = 'rgb(254, 169, 17)';
    store.data[win].backgroundInner = 'rgb(254, 169, 17)';
    store.data[win].borderColor = 'rgb(254, 169, 17)';
    store.data[win].color = 'rgb(47, 61, 87)';
    store.data[win].shadow = '';
    store.data[win].gradientInner = '';
    store.data[win].shadowInner = '';
    if (this.state.grid.data[win].connected.length) {
      this.state.grid.data[win].connected.forEach(item => {
        !this.state.grid.data[item].amount && this.commit('grid/CLEAN_ONE_CELL', item);
      })
    }
  },
  SELECT_CELL_PLATFORM(store, win) {
    store.data[win].background = 'rgb(0, 179, 189)';
    store.data[win].backgroundInner = 'rgb(0, 179, 189)';
    store.data[win].borderColor = 'rgb(0, 179, 189)';
    store.data[win].color = '#2f3d57';
    store.data[win].shadow = '';
    store.data[win].gradientInner = '';
    store.data[win].shadowInner = '';
    if (this.state.grid.data[win].connected.length) {
      this.state.grid.data[win].connected.forEach(item => {
        !this.state.grid.data[item].amount && this.commit('grid/CLEAN_ONE_CELL', item);
      })
    }
  },
  SELECT_CELL_NOBODY(store, win) {
    store.data[win].background = colorMixer.changeBackgroundColor(colorMixer.maxBet);
    store.data[win].backgroundInner = colorMixer.changeBackgroundColor(colorMixer.maxBet);
    store.data[win].shadow = colorMixer['shadow'].default + ' ' + colorMixer.changeShadow(colorMixer.maxBet);
    store.data[win].borderColor = colorMixer.changeBorder(colorMixer.maxBet);
    if (this.state.grid.data[win].connected.length) {
      this.state.grid.data[win].connected.forEach(item => {
        !this.state.grid.data[item].amount && this.commit('grid/CLEAN_ONE_CELL', item);
      })
    }
  },
  CLEAN_ONE_CELL(store, lastWin) {
    store.data[lastWin].amount = 0;
    store.data[lastWin].background = '';
    store.data[lastWin].gradientInner = '';
    store.data[lastWin].backgroundInner = '';
    store.data[lastWin].borderColor = '';
    store.data[lastWin].borderWidth = 2;
    store.data[lastWin].shadow = '';
    store.data[lastWin].shadowInner = '';
    store.data[lastWin].color = '';
  },
  CLEAN_GRID(store) {
    Object.keys(store.data).forEach(item => {
      this.commit('grid/CLEAN_ONE_CELL', item)
    });
  },
  SET_CELL_SHADOW(store, {number}) {
    store.shadowWin = number;
  },
  SET_CELL_WAITING(store, number) {
    store.waiting.push(number);
  },
  REMOVE_CELL_WAITING(store, number) {
    if (typeof number === 'object') {
      store.waiting = [];
    } else {
      if (store.waiting.includes(number)) {
        store.waiting.splice(store.waiting.indexOf(number), 1);
      }
    }
  }
};
export const actions = {
  setWaiting(context, number) {
    context.commit('SET_CELL_WAITING', number[0]);
  },
  removeWaiting(context, number) {
    context.commit('REMOVE_CELL_WAITING', number[0]);
  },
  set(context, {amount = 0, number = [], my = true, linked = true, maxBet} = {}) {
    context.commit('SET_GRID_SETTINGS', {
      key: number[0],
      amount,
      my,
      linked,
      maxBet
    })
  },
  setShadowWin(context, {number}) {
    context.commit('SET_CELL_SHADOW', {number});
  },
  clean(context) {

  },
  finishGame(context, win) {
    return new Promise((resolve, reject) => {
      const betsQueue = [];

      /**
       * Need to optimizate
       */
      const myBets = [];
      const otherBets = [];
      context.rootState.game.bets.my.forEach(item => {
        myBets.push(item.number[0] + '');
      });
      context.rootState.game.bets.all.forEach(item => {
        otherBets.push(item.number[0] + '');
      });


      const winCells = [win];
      if (!betsQueue.length) {
        Object.keys(context.state.data).forEach(item => {
          if (parseInt(item) !== win && context.state.data[item].amount) betsQueue.push(item);
          if (context.state.data[item].connected.includes(win)) winCells.push(item);
        });
        betsQueue.push(win);
      }


      betsQueue.forEach((win, index) => {
        const lastWin = index !== 0 ? (betsQueue[index - 1] + '') : 'no';
        if (lastWin !== 'no') {
          let action = 'CLEAN_ONE_CELL';
          if (myBets.includes(lastWin)) action = 'SELECT_CELL_LOSE';
          if (winCells.includes(lastWin) && myBets.includes(lastWin)) action = 'SELECT_CELL_WIN';
          if (!myBets.includes(lastWin) && otherBets.includes(lastWin)) action = 'SELECT_CELL_OTHER';
          if (!myBets.includes(lastWin) && otherBets.includes(lastWin) && !winCells.includes(lastWin)) action = 'SELECT_CELL_PLATFORM';
          if (!myBets.includes(lastWin) && !otherBets.includes(lastWin) && !otherBets.length && !myBets.length) action = 'SELECT_CELL_NOBODY';
          context.commit(action, lastWin);
        }
        if (index !== betsQueue.length - 1) {
          context.commit('CHECK_CELL_WIN', win);
        } else {
          let action = 'SELECT_CELL_NOBODY';
          if (myBets.includes(win)) action = 'SELECT_CELL_LOSE';
          if (winCells.includes(win) && myBets.includes(win)) action = 'SELECT_CELL_WIN';
          if (!myBets.includes(win) && otherBets.includes(win)) action = 'SELECT_CELL_OTHER';
          if (!myBets.includes(win) && !otherBets.includes(win) && !winCells.includes(win)) action = 'SELECT_CELL_PLATFORM';
          if (!myBets.includes(win) && !otherBets.includes(win) && !winCells.includes(win) && !otherBets.length && !myBets.length) action = 'SELECT_CELL_NOBODY';
          context.commit(action, win);
          setTimeout(_ => {
            resolve(winCells);
          }, 1000);
        }
      });
    })
  }
};
